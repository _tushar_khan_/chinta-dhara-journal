<?php
use App\Models\Tag;
use Faker\Generator as Faker;

$factory->define(Tag::class, function (Faker $faker) {
    return [
        "name" => $faker->name($faker->randomElement(["male", "female"])),
        "slug" => trim($faker->word),
        "status" => $faker->numberBetween(1, 3)
    ];
});