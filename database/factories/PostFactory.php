<?php

use App\Models\Post;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        "title" => $faker->title,
        "body" => $faker->paragraph,
        "status" => $faker->numberBetween(0, 2),
        "slug" => trim($faker->word),
        "post_type" => "article",
        "comment_status" => 1
    ];
});