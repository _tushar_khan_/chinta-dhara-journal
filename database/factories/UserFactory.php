<?php

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => 'Super Admin',
        'email' => 'admin@devxpart.com',
        'password' => '$2y$10$50sKq9q2cOS0.phgaFkv..F.Eb1plpGzTJu//NKJnNzOMoY9B9fMe', // admin
    ];
});

// INSERT INTO `users`(`name`, `username`, `email`, `password`, `role`) VALUES ('Al-Amin Firdows', 'admin', 'alaminfirdows@gmail.com', '$2y$10$.nGfcZ7J429QjREmToQWJeZqyz2hGRIJW/uniR7suw6wX7PWr/WTq', 1);