<?php
use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        "name" => $faker->name($faker->randomElement(["male", "female"])),
        "slug" => trim($faker->word),
        "status" => $faker->numberBetween(1, 3)
    ];
});