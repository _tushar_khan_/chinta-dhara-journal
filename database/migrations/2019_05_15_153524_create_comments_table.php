<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('post_id')->nullable()->default(0);
            $table->foreign('post_id')->references('id')->on('posts')->onUpdate('cascade')->onDelete('set null');
            $table->string('author_name')->nullable(true);
            $table->string('user_id')->default(0);
            $table->text('body');
            $table->string('type')->default(1);
            $table->unsignedBigInteger('parent_id')->default(0)->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('comments');
    }
}