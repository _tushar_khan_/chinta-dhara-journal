<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text("title");
            $table->text("body")->nullable();
            $table->integer("status")->default(1);
            $table->string("slug")->nullable();
            $table->string("thumbnail")->nullable();
            $table->string("post_type")->default('article');
            $table->integer("views")->default(0);
            $table->integer("likes")->default(0);
            $table->integer("comment_status")->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
