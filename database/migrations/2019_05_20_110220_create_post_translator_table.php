<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosttranslatorTable extends Migration
{

    public function up()
    {
        Schema::create('post_translator', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("translator_id");
            $table->foreign("translator_id")->references("id")->on("translators")->onUpdate("cascade")->onDelete('cascade');
            $table->unsignedBigInteger("post_id");
            $table->foreign("post_id")->references("id")->on("posts")->onUpdate("cascade")->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('post_translator');
    }
}