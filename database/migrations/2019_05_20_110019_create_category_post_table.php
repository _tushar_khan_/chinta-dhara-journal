<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryPostTable extends Migration
{

    public function up()
    {
        Schema::create('category_post', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("post_id");
            $table->unsignedBigInteger("category_id");
            $table->foreign("post_id")->references("id")->on("posts")->onUpdate("cascade")->onDelete('cascade');
            $table->foreign("category_id")->references("id")->on("categories")->onUpdate("cascade")->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('category_post');
    }
}