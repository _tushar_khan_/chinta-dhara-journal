<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('username')->unique()->nullable();
            $table->string('email')->unique();
            $table->string('phone')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('role')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}

// INSERT INTO `users` (`id`, `name`, `username`, `email`, `phone`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES (NULL, 'Al-Amin', 'alamin', 'alaminfirdows@gmail.com', '01740450457', NULL, '$2y$10$VbNY2SeRrEbLwelpjrKBgOCPta27iit4Xqfjxd4RLgosr9jswcx2i', '1', NULL, NULL, NULL);

// INSERT INTO `users` (`id`, `name`, `username`, `email`, `phone`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES (NULL, 'Super Admin', 'admin', 'admin@devxpart.com', '01740450457', NULL, '$2y$10$50sKq9q2cOS0.phgaFkv..F.Eb1plpGzTJu//NKJnNzOMoY9B9fMe', '1', NULL, NULL, NULL);

// INSERT INTO `users` (`id`, `name`, `username`, `email`, `phone`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES (NULL, 'এডমিন', 'admin', 'admin@chintadhara.com', '01740450457', NULL, '$2y$10$Q8fSVNqg0XMQJ4QVb26uR.8gLjBwC6FW64o9N3dJPfdvPz0BdY.uG', '1', NULL, NULL, NULL);