<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostRequestsTable extends Migration
{
    public function up()
    {
        Schema::create('post_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text("author_name");
            $table->text("author_phone");
            $table->text("author_email");
            $table->text("title");
            $table->text("body")->nullable();
            $table->integer("status")->default(0);
            $table->string("thumbnail")->nullable();
            $table->string("post_type")->default('article');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('post_requests');
    }
}
