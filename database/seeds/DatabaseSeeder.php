<?php

// use App\Models\Category;
// use App\Models\Post;
// use App\Models\Tag;
// use App\User;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Tag::class, 3)->create();
        factory(Category::class, 3)->create();
        factory(User::class, 1)->create();
        factory(Post::class, 5)->create();
    }
}
