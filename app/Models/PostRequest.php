<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostRequest extends Model
{
    protected $fillable = [];

    protected $attributes = [
        'status' => 0
    ];

    protected $guarded = [];

    public function getStatusAttribute($attribute)
    {
        return $this->statusOptions()[$attribute];
    }

    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }

    public function scopePending($query)
    {
        return $query->where('status', 0);
    }

    public function statusOptions()
    {
        return [
            1 => 'Published',
            0 => 'Pending'
        ];
    }
}