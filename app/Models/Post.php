<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = array("title", "body", "status", "user_id", "slug", "thumbnail", "post_type", "comment_status");

    protected $attributes = [
        'status' => 1
    ];

    public function getStatusAttribute($attribute)
    {
        return $this->statusOptions()[$attribute];
    }

    public function getPostTypeAttribute($attribute)
    {
        return $this->postTypeOptions()[$attribute];
    }

    public function scopePublished($query)
    {
        return $query->where('status', 1);
    }

    public function scopeUnPublished($query)
    {
        return $query->where('status', 0);
    }

    public function scopeDraft($query)
    {
        return $query->where('status', 2);
    }

    public function scopeArticle($query)
    {
        return $query->where('post_type', 'article');
    }
    public function scopeVideo($query)
    {
        return $query->where('post_type', 'video');
    }
    public function scopeAudio($query)
    {
        return $query->where('post_type', 'audio');
    }

    public function statusOptions()
    {
        return [
            1 => 'Published',
            0 => 'Unpublished',
            2 => 'Draft'
        ];
    }

    public function postTypeOptions()
    {
        return [
            'article' => 'Article',
            'video' => 'Video',
            'audio' => 'Audio',
        ];
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    public function translators()
    {
        return $this->belongsToMany(Translator::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->where('type', '1');
    }
}