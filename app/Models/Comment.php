<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = array('post_id', 'name', 'body', 'type', 'parent_id', 'status');

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    // public function parent()
    // {
    //     return $this->belongsTo(Post::class, 'parent_id');
    // }

    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id', 'id')->where('type', '2');
    }
}