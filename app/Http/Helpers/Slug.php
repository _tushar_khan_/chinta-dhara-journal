<?php
class Slug
{
    static function checkSlug($slug)
    {
        $all_slugs = Post::select('slug')->where('slug', 'like', $slug . '%')->get();

        if (!$all_slugs->contains('slug', $slug)) {
            return $slug;
        }
        $i = 1;
        while ($i++) {
            $new_slug = $slug . '-' . $i;
            if (!$all_slugs->contains('slug', $new_slug)) {
                return $new_slug;
            }
        }
    }

    static function createSlug($title)
    {
        $slug_text = Str::words($title, 15, '');
        return self::checkSlug(mb_strtolower(preg_replace('/[ ,_\/@=]+/', '-', trim($slug_text)), 'UTF-8'));
    }
}