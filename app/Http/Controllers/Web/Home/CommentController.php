<?php

namespace App\Http\Controllers\Web\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;
use Session;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $validated = $request->validate([
            'post_id' => 'required',
            'author_name' => 'sometimes',
            'body' => 'required',
        ]);

        if ($validated == true) {
            $comment = new Comment();
            $comment->post_id = $request->input("post_id");
            $comment->author_name = $request->input("author_name");
            $comment->body = $request->input("body");

            if ($comment->save()) {
                Session::flash('comment_response', array('type' => 'success', 'message' => 'Comment added successfully!'));
            } else {
                Session::flash('comment_response', array('type' => 'error', 'message' => 'Something Went wrong!'));
            }
        } else {
            Session::flash('comment_response', array('type' => 'error', 'message' => 'Data not valid!'));
        }

        return back();
    }

    public function reply_store(Comment $comment, Request $request)
    {
        $validated = $request->validate([
            'post_id' => 'required',
            'author_name' => 'sometimes',
            'body' => 'required',
        ]);

        if ($validated == true) {
            $reply = new Comment();
            $reply->post_id = $request->input("post_id");
            $reply->user_id = Auth()->user() ? Auth()->user()->id : '0';
            $reply->author_name =  $request->input("author_name") ? $request->input("author_name") : (Auth()->user() ? Auth()->user()->name : '');
            $reply->body = $request->input("body");
            $reply->type = 2;
            $reply->parent_id = $comment->id;

            if ($reply->save()) {
                Session::flash('comment_response', array('type' => 'success', 'message' => 'Comment added successfully!'));
            } else {
                Session::flash('comment_response', array('type' => 'error', 'message' => 'Something Went wrong!'));
            }
        } else {
            Session::flash('comment_response', array('type' => 'error', 'message' => 'Data not valid!'));
        }

        return back();
    }
}