<?php

namespace App\Http\Controllers\Web\Home;

use App\Http\Controllers\Controller;
use App\Models\Author;

class AuthorController extends Controller
{
    public function index()
    {
        $data = array(
            'authors' => Author::active()->select('id', 'name', 'slug', 'image')->orderBy('id', 'DESC')->take(6)->get()
        );
        return view('home.authors.index', $data);
    }

    public function show(Author $author)
    {
        $data = array(
            'author' => Author::active()->findOrFail($author->id),
            'posts' => Author::active()->with('publishedPosts.categories')->findOrFail($author->id)->publishedPosts
        );

        return view('home.authors.show', $data);
    }
}