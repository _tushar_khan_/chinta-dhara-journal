<?php

namespace App\Http\Controllers\Web\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\Author;
use App\Models\PostRequest;
use Session;

class HomeController extends Controller
{
    public function index()
    {
        $data = array(
            'recent_posts' => Post::published()->orderBy('id', 'DESC')->with(['categories', 'authors', 'translators'])->take(6)->get(),
            'cat_1' => array(
                'title' => Category::find(1) ? Category::select('name', 'slug')->find(1) : ['name' => 'চিন্তাধারা'],
                'posts' => Category::find(1) ? Category::find(1)->publishedPosts->take(3) : null,
            ),
            'cat_2' => array(
                'title' => Category::find(2) ? Category::select('name', 'slug')->find(2) : ['name' => 'চিন্তাধারা'],
                'posts' => Category::find(2) ? Category::find(2)->publishedPosts->take(3) : null,
            ),
            'cat_3' => array(
                'title' => Category::find(3) ? Category::select('name', 'slug')->find(3) : ['name' => 'চিন্তাধারা'],
                'posts' => Category::find(3) ? Category::find(3)->publishedPosts->take(3) : null,
            ),
            'authors' => Author::active()->select('id', 'name', 'slug', 'image')->orderBy('id', 'DESC')->take(6)->get(),
        );

        //return $data['recent_posts'];

        return view('home.index', $data);
    }

    public function feeds()
    {
        $data = array();
        return view('home.feeds', $data);
    }

    public function socials()
    {
        $data = array();
        return view('home.socials', $data);
    }

    public function post_box()
    {
        $data = array();
        return view('home.post_box', $data);
    }

    public function post_box_store(Request $request)
    {
        $post_data = request()->validate([
            'author_name' => 'required|min:3',
            'author_phone' => 'sometimes',
            'author_email' => 'sometimes',
            'title' => 'required|min:3',
            'body' => 'required|min:100'
        ]);

        if ($post_data) {
            if (PostRequest::create($post_data)) {
                Session::flash('response', array('type' => 'success', 'message' => 'Post Request send successfully!'));
            } else {
                Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
            }
            return redirect(route('post_box'));
        }
        return back()->withInput();
    }
}