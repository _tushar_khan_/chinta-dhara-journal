<?php

namespace App\Http\Controllers\Web\Home;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Session;

class ArticleController extends Controller
{
    public function index()
    {
        $data = array(
            "articles" => Post::published()->article()->with(['categories', 'tags', 'authors', 'translators'])->get()
        );
        return view("home.articles.index", $data);
    }

    public function show(Post $article)
    {
        // increment view count
        $article->increment('views', 1);

        $data = array(
            'article' => Post::published()->with(['categories', 'tags', 'authors', 'translators', 'comments', 'comments.replies'])->findOrFail($article->id),
            "related_articles" => Post::published()->where('id', $article->id)->with(['authors'])->inRandomOrder()->take(4)->get()
        );
        //return $data;
        return view("home.articles.show", $data);
    }

    public function showBySlug($slug)
    {
        // Find post by slug
        $article = Post::published()->where('slug', $slug)->with(['categories', 'tags', 'authors', 'translators', 'comments', 'comments.replies'])->firstOrFail();

        // increment view count
        $article->increment('views', 1);

        $data = array(
            'article' => $article,
            "related_articles" => Post::published()->where('id', $article->id)->with(['authors'])->inRandomOrder()->take(4)->get()
        );
        return view("home.articles.show", $data);
    }
}