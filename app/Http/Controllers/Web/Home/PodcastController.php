<?php

namespace App\Http\Controllers\Web\Home;

use App\Http\Controllers\Controller;

class PodcastController extends Controller
{
    public function index()
    {
        $data = array();

        // return $data;

        return view('home.podcasts.index', $data);
    }
}