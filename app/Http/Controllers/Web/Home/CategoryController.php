<?php

namespace App\Http\Controllers\Web\Home;

use App\Models\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        //
    }

    public function show(Category $category)
    {
        $data = array(
            'category' => Category::find($category->id),
            'posts' => Category::with('publishedPosts.authors', 'publishedPosts.tags', 'publishedPosts.translators')->find($category->id)->publishedPosts
        );
        return view('home.categories.show', $data);
    }
}
