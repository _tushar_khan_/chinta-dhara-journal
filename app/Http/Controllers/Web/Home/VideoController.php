<?php

namespace App\Http\Controllers\Web\Home;

use App\Http\Controllers\Controller;
use App\Models\Post;

class VideoController extends Controller
{
    public function index()
    {
        $data = array();

        // return $data;

        return view('home.videos.index', $data);
    }

    public function show(Post $video)
    {
        $video->increment('views', 1);
        return $video;
    }
}