<?php

namespace App\Http\Controllers\Web\Home;

use App\Http\Controllers\Controller;
use App\Models\Translator;

class TranslatorController extends Controller
{
    public function index()
    {
        $data = array(
            'translators' => Translator::all()
        );
        return view('home.translators.index', $data);
    }

    public function show(Translator $translator)
    {
        $data = array(
            'translator' => Translator::find($translator->id),
            'posts' => Translator::with('posts.categories', 'posts.tags', 'posts.translators')->find($translator->id)->posts
        );
        return view('home.translators.show', $data);
    }
}