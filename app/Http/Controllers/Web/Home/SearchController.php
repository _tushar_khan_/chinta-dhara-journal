<?php

namespace App\Http\Controllers\Web\Home;

use App\Http\Controllers\Controller;
use App\Models\Author;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\Translator;

class SearchController extends Controller
{
    public function index()
    {
        $data = array();
        return view('home.search.index', $data);
    }


    public function search(Request $request)
    {
        $key = $request->input('query');
        if ($key) {
            $data = array(
                'results' => array(
                    'posts' => Post::with(['authors', 'translators'])->select('id', 'title', 'status', 'slug', 'thumbnail', 'post_type', 'views')->where('title', 'like', '%' . $key . '%')->where('status', '=', 1)->get(),
                    // 'categories' => Category::select('name', 'slug', 'thumbnail', 'status')->where('name', 'like', '%' . $key . '%')->where('status', '=', 1)->get(),
                    // 'authors' => Author::select('name', 'slug', 'image', 'status')->where('name', 'like', '%' . $key . '%')->where('status', '=', 1)->get(),
                    // 'translators' => Translator::select('name', 'slug', 'image', 'status')->where('name', 'like', '%' . $key . '%')->where('status', '=', 1)->get()
                ),
                'key' => $key
            );

            return view('home.search.result', $data);
        } else {
            return redirect(route('search.index'));
        }
    }
}