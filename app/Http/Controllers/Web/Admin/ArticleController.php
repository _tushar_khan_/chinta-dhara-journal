<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use Session;
use App\Models\Post;
use App\Models\Category;
use App\Models\Author;
use App\Models\Translator;
use App\Models\Comment;

class ArticleController extends Controller
{
    public function index()
    {
        $data = array(
            "articles" => Post::where('post_type', 'article')->with(['categories', 'tags', 'authors', 'translators', 'comments'])->orderBy('id', 'desc')->paginate(500)
        );
        return view("admin.articles.index", $data);
    }

    public function create()
    {
        $data = array(
            "categories" => Category::select('id', 'name')->where('status', 1)->get(),
            "authors" => Author::select('id', 'name')->active()->get(),
            "translators" => Translator::select('id', 'name')->where('status', 1)->get(),
        );
        return view("admin.articles.create", $data);
    }

    public function store(Request $request)
    {
        $article = new Post();
        $article->title = $request->input("title");
        $article->body = $request->input("body");
        $article->slug = $request->has('slug') && !empty($request->input("slug")) ? $this->createSlug($request->input("slug")) : $this->createSlug($request->input("title"));
        $article->status = $request->has('status') ? $request->input("status") : 1;
        $article->post_type = "article";
        if ($request->hasFile("thumbnail")) {
            $image = $request->file('thumbnail');
            $timestemp = time();
            //$featured_image = 'image_' . $timestemp . '.' . $image->getClientOriginalExtension();
            //$path = storage_path() . '/app/public/uploads/post/featured_image/' . $featured_image;

            $image_name = $timestemp . '.' . $image->getClientOriginalExtension();
            $path = public_path('storage/uploads/post/featured_image/') . 'image_' . $image_name;
            Image::make($image)->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path);

            //$thumbnail = 'thumb_' . $timestemp . '.' . $image->getClientOriginalExtension();
            //$path = storage_path() . '/app/public/uploads/post/thumbnail/' . $thumbnail;
            $path = public_path('storage/uploads/post/thumbnail/') . 'thumb_' . $image_name;
            Image::make($image)->fit(350, 240)->save($path);

            $article->thumbnail = $image_name;
        }

        if ($article->save()) {
            $article->categories()->sync($request->categories);
            //$request->has('categories') ? $article->categories()->sync($request->categories) : $article->categories()->attach(1);
            // $article->tag()->sync($request->tags);
            $article->authors()->sync($request->authors);
            $article->translators()->sync($request->translators);
            Session::flash('response', array('type' => 'success', 'message' => 'Post added successfully!'));
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
        }
        return redirect(route("admin.articles.index"));
    }


    public function show(Post $post)
    {
        $data = array(
            "post" => Post::with(["categories", "tags"])->find($post->id)
        );
        return view("admin.articles.view", $data);
    }

    public function edit(Post $article)
    {
        $data = array(
            "article" => Post::with(["categories", "tags"])->find($article->id),
            "categories" => Category::select('id', 'name')->where('status', 1)->get(),
            "authors" => Author::select('id', 'name')->where('status', 1)->get(),
            "translators" => Translator::select('id', 'name')->where('status', 1)->get(),
        );
        return view("admin.articles.edit", $data);
    }

    public function update(Request $request, Post $article)
    {
        $article->title = $request->input("title");
        $article->body = $request->input("body");
        if ($request->has('slug') && $request->input("slug") != $article->slug) {
            $article->slug = !empty($request->input("slug")) ? $this->createSlug($request->input("slug")) : $this->createSlug($request->input("title"));
        }
        $article->status = ($request->has('status')) ? $request->input("status") : 1;
        $article->updated_at = date("Y-m-d h:m:s");
        if ($request->hasFile("thumbnail")) {
            $image = $request->file('thumbnail');
            $timestemp = time();
            //$featured_image = 'image_' . $timestemp . '.' . $image->getClientOriginalExtension();
            //$path = storage_path() . '/app/public/uploads/post/featured_image/' . $featured_image;

            $image_name = $timestemp . '.' . $image->getClientOriginalExtension();
            $path = public_path('storage/uploads/post/featured_image/') . 'image_' . $image_name;
            Image::make($image)->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path);

            //$thumbnail = 'thumb_' . $timestemp . '.' . $image->getClientOriginalExtension();
            //$path = storage_path() . '/app/public/uploads/post/thumbnail/' . $thumbnail;
            $path = public_path('storage/uploads/post/thumbnail/') . 'thumb_' . $image_name;
            Image::make($image)->fit(350, 240)->save($path);

            $article->thumbnail = $image_name;
        }
        if ($article->save()) {
            $article->categories()->sync($request->categories);
            //$request->has('categories') ? $article->categories()->sync($request->categories) : $article->categories()->attach(1);
            // $article->tag()->sync($request->tags);
            $article->authors()->sync($request->authors);
            $article->translators()->sync($request->translators);
            Session::flash('response', array('type' => 'success', 'message' => 'Post updated successfully!'));
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
        }
        return redirect(route("admin.articles.index"));
    }

    public function destroy(Post $article)
    {
        $article = Post::with(['categories', 'tags', 'authors', 'translators', 'comments'])->find($article->id);

        foreach ($article->categories as $category) {
            $article->categories()->detach($category->id);
        }
        foreach ($article->tags as $tag) {
            $article->tags()->detach($tag->id);
        }

        foreach ($article->authors as $author) {
            $article->authors()->detach($author->id);
        }

        foreach ($article->translators as $translator) {
            $article->translators()->detach($translator->id);
        }
        Comment::where('post_id', $article->id)->delete();

        if ($article->delete()) {
            Session::flash('response', array('type' => 'success', 'message' => 'Post deleted successfully!'));
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
        }
        return redirect(route("admin.articles.index"));
    }

    // public function uploadimage(Request $request)
    // {
    //     if ($request->hasFile("upload")) {
    //         $image = $request->file('upload');
    //         $filename = 'image_' . time() . '.' . $image->getClientOriginalExtension();
    //         //$path = storage_path() . '/app/public/uploads/article-image/' . $filename;
    //         $path = public_path('storage/uploads/post/post_image/') . $filename;
    //         Image::make($image)->resize(800, null, function ($constraint) {
    //             $constraint->aspectRatio();
    //         })->save($path);

    //         $image_url = url('/storage/uploads/post/post_image/' . $filename);
    //         echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' . $request->CKEditorFuncNum . '", "' . $image_url . '", "");</script>';
    //     }
    // }

    private function checkSlug($slug)
    {
        $all_slugs = Post::select('slug')->where('slug', 'like', $slug . '%')->get();

        if (!$all_slugs->contains('slug', $slug)) {
            return $slug;
        }
        $i = 1;
        while ($i++) {
            $new_slug = $slug . '-' . $i;
            if (!$all_slugs->contains('slug', $new_slug)) {
                return $new_slug;
            }
        }
    }

    private function createSlug($title)
    {
        $slug_text = Str::words($title, 15, '');
        return $this->checkSlug(mb_strtolower(preg_replace('/[ ,.@#$%^&*()_\/=]+/', '-', trim($slug_text)), 'UTF-8'));
    }



    public function uploadimage(Request $request)
    {
        if ($request->hasFile("image")) {
            $image = $request->file('image');
            $filename = 'image_' . time() . '.' . $image->getClientOriginalExtension();
            //$path = storage_path() . '/app/public/uploads/article-image/' . $filename;
            $path = public_path('storage/uploads/post/post_image/') . $filename;
            Image::make($image)->resize(800, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($path);

            $image_url = url('/storage/uploads/post/post_image/' . $filename);
            echo $image_url;
        }
    }
}