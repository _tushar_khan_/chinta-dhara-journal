<?php

namespace App\Http\Controllers\Web\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;


class CategoryController extends Controller
{
    public function index()
    {
        $data = array(
            'categories' => Category::orderBy('id', 'desc')->get()
        );
        return view('admin.categories.index', $data);
    }

    public function create()
    {
        $data = array();
        return view('admin.categories.create', $data);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
        ]);

        if ($validated == true) {
            $categories = new Category();
            $categories->name = $request->name;
            $categories->slug = str_slug($request->name);
            $categories->status = $request->status;

            if ($categories->save()) {
                Session::flash('response', array('type' => 'success', 'message' => 'Category added successfully!'));
            } else {
                Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
            }
            return redirect('admin/categories');
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Data not valid!'));
            return redirect('admin/categories/create');
        }
    }

    public function show(Category $category)
    {
        //
    }

    public function edit(Category $category)
    {
        $data = array(
            'category' => Category::find($category->id)
        );
        return view('admin/categories/edit', $data);
    }

    public function update(Request $request, $id)
    {
        $categories = Category::find($id);
        $categories->name = $request->name;
        $categories->status = $request->status;
        $categories->save();

        if ($categories->save()) {
            Session::flash('response', array('type' => 'success', 'message' => 'categories Updated successfully!'));
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
        }
        return redirect('admin/categories');
    }

    public function destroy(Category $category)
    {
        if ($category->delete()) {
            Session::flash('response', array('type' => 'success', 'message' => 'Category deleted successfully!'));
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
        }
        return redirect('admin/categories');
    }
}