<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use Image;
use Session;
use Auth;

class PostController extends Controller
{
    public function index()
    {

        $data = array(
            "posts" => Post::with(["category", "tag"])->where("user_id", Auth::guard('writer')->user()->id)->get()
        );

        return view("writer.post.posts", $data);
    }

    public function create()
    {
        $data = array(
            "categories" => Category::all(['id', 'name', 'slug', 'status'])->where("status", 1),
            // "tags" => Tag::all(['id', 'name', 'status'])->where("status", 1)
        );
        return view("writer.post.create", $data);
    }

    public function store(Request $request)
    {
        $post = new Post();
        $post->title = $request->input("title");
        $post->body = $request->input("body");
        $post->slug = $request->input("title");
        $post->status = $request->input("status");
        $post->user_id = Auth::guard('writer')->user() ? Auth::guard('writer')->user()->id : 1;
        $post->post_type = "article";
        $post->updated_at = date("Y-m-d h:m:s");
        if ($request->hasFile("thumbnail")) {
            $image = $request->file('thumbnail');
            $filename = 'thumb_' . time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('/uploads/post/thumbnail/' . $filename);

            Image::make($image)->resize(320, 240)->save($path);
            $post->thumbnail = $filename;
        }
        if ($post->save()) {
            $post->category()->sync($request->categories);
            $post->tag()->sync($request->tags);
            Session::flash('response', array('type' => 'success', 'message' => 'Post added successfully!'));
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
        }
        return redirect(route("writer.posts.index"));
    }


    public function show(Post $post)
    {
        $data = array(
            "post" => Post::with(["category", "tag"])->where("user_id", Auth::guard('writer')->user()->id)->find($post->id)
        );

        return view("writer.posts.view", $data);
    }

    public function edit(Post $post)
    {
        $data = array(
            "post" => Post::with(["category", "tag"])->where("user_id", Auth::guard('writer')->user()->id)->find($post->id),
            "categories" => Category::all(['id', 'name', 'slug', 'status'])->where("status", 1),
            // "tags" => Tag::all(['id', 'name', 'status'])->where("status", 1)
        );

        // return $data;
        return view("writer.post.edit", $data);
    }

    public function update(Request $request, Post $post)
    {
        $post->title = $request->input("title");
        $post->body = $request->input("body");
        $post->slug = $request->input("title");
        $post->status = $request->input("status");
        $post->post_type = "article";
        $post->updated_at = date("Y-m-d h:m:s");
        if ($request->hasFile("thumbnail")) {
            $image = $request->file('thumbnail');
            $filename = 'thumb_' . time() . '.' . $image->getClientOriginalExtension();
            $path = public_path('/uploads/post/thumbnail/' . $filename);

            Image::make($image)->resize(320, 240)->save($path);
            $post->thumbnail = $filename;
        }
        if ($post->save()) {
            $post->category()->sync($request->categories);
            $post->tag()->sync($request->tags);
            Session::flash('response', array('type' => 'success', 'message' => 'Post updated successfully!'));
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
        }
        return redirect(route("writer.posts.index"));
    }

    public function destroy(Post $post)
    {
        $post = Post::with(["category", "tag"])->where("user_id", Auth::guard('writer')->user()->id)->find($post->id);
        foreach ($post->tag as $tag) {
            $post->tag()->detach($tag->id);
        }
        foreach ($post->category as $category) {
            $post->category()->detach($category->id);
        }

        if ($post->delete()) {
            Session::flash('response', array('type' => 'success', 'message' => 'Post deleted successfully!'));
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
        }
        return redirect(route("writer.posts.index"));
    }
}