<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use Session;
use App\Models\Translator;

class TranslatorController extends Controller
{
    public function index()
    {
        $data = array(
            'translators' => Translator::orderBy('name', 'asc')->paginate(100)
        );
        return view('admin.translators.index', $data);
    }

    public function create()
    {
        $data = array(
            'translator' => new Translator()
        );
        return view('admin.translators.create', $data);
    }

    public function store(Request $request)
    {
        $translator_data = $this->validateRequest();
        if ($translator_data) {
            $translator_data['slug'] = $request->has('slug') && !empty($request->input("slug")) ? $this->createSlug($request->input("slug")) : $this->createSlug($request->input("name"));

            if ($request->hasFile("image")) {
                $translator_data['image'] = $this->uploadImage($request->file('image'));
            }

            if (Translator::create($translator_data)) {
                Session::flash('response', array('type' => 'success', 'message' => 'Translator added successfully!'));
            } else {
                Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
            }
            return redirect(route('admin.translators.index'));
        }
        return redirect(route('admin.translators.create'));
    }

    public function show(Translator $translator)
    {
        //
    }

    public function edit(Translator $translator)
    {
        $data = array(
            'translator' => $translator
        );
        return view('admin.translators.edit', $data);
    }

    public function update(Request $request, Translator $translator)
    {
        $translator_data = $this->validateRequest();
        if ($translator_data) {
            if ($request->has('slug') && $request->input("slug") != $translator->slug) {
                $translator_data['slug'] = !empty($request->input("slug")) ? $this->createSlug($request->input("slug")) : $this->createSlug($request->input("name"));
            }

            if ($request->hasFile("image")) {
                $translator_data['image'] = $this->uploadImage($request->file('image'));
            }

            if ($translator->update($translator_data)) {
                Session::flash('response', array('type' => 'success', 'message' => 'Translator updated successfully!'));
            } else {
                Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
            }
            return redirect(route('admin.translators.index'));
        }
        return redirect(route('admin.translators.edit', $translator->id));
    }

    public function destroy(Translator $translator)
    {
        if ($translator->delete()) {
            Session::flash('response', array('type' => 'success', 'message' => 'Translator deleted successfully!'));
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
        }
        return redirect(route('admin.translators.index'));
    }

    private function validateRequest()
    {
        return request()->validate([
            'name' => 'required|min:3',
            'bio' => 'sometimes',
            'status' => 'required',
            'image' => 'sometimes|file|image|max:5000',
        ]);
    }

    private function checkSlug($slug)
    {
        $all_slugs = Translator::select('slug')->where('slug', 'like', $slug . '%')->get();

        if (!$all_slugs->contains('slug', $slug)) {
            return $slug;
        }
        $i = 1;
        while ($i++) {
            $new_slug = $slug . '-' . $i;
            if (!$all_slugs->contains('slug', $new_slug)) {
                return $new_slug;
            }
        }
    }

    private function createSlug($title)
    {
        $slug_text = Str::words($title, 15, '');
        return $this->checkSlug(mb_strtolower(preg_replace('/[ ,_\/@=]+/', '-', trim($slug_text)), 'UTF-8'));
    }

    private function uploadImage($image)
    {
        $timestemp = time();
        $image_name = $timestemp . '.' . $image->getClientOriginalExtension();
        $path = public_path('storage/uploads/profile_picture/translator/') . 'image_' . $image_name;
        Image::make($image)->fit(400, 400)->save($path);
        return $image_name;
    }
}