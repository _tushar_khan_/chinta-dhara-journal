<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use Session;
use App\Models\Author;

class AuthorController extends Controller
{
    public function index()
    {
        $data = array(
            'authors' => Author::orderBy('id', 'desc')->paginate(100)
        );
        return view('admin.authors.index', $data);
    }

    public function create()
    {
        $data = array(
            'author' => new Author()
        );
        return view('admin.authors.create', $data);
    }

    public function store(Request $request)
    {
        $author_data = $this->validateRequest();
        if ($author_data) {
            $author_data['slug'] = $request->has('slug') && !empty($request->input("slug")) ? $this->createSlug($request->input("slug")) : $this->createSlug($request->input("name"));

            if ($request->hasFile("image")) {
                $author_data['image'] = $this->uploadImage($request->file('image'));
            }

            if (Author::create($author_data)) {
                Session::flash('response', array('type' => 'success', 'message' => 'Author added successfully!'));
            } else {
                Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
            }
            return redirect(route('admin.authors.index'));
        }
        return redirect(route('admin.authors.create'));
    }

    public function show(Author $author)
    {
        //
    }

    public function edit(Author $author)
    {
        $data = array(
            'author' => $author
        );
        return view('admin.authors.edit', $data);
    }

    public function update(Request $request, Author $author)
    {
        $author_data = $this->validateRequest();
        if ($author_data) {
            if ($request->has('slug') && $request->input("slug") != $author->slug) {
                $author_data->slug = !empty($request->input("slug")) ? $this->createSlug($request->input("slug")) : $this->createSlug($request->input("name"));
            }

            if ($request->hasFile("image")) {
                $author_data['image'] = $this->uploadImage($request->file('image'));
            }

            if ($author->update($author_data)) {
                Session::flash('response', array('type' => 'success', 'message' => 'Author updated successfully!'));
            } else {
                Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
            }
            return redirect(route('admin.authors.index'));
        }
        return redirect(route('admin.authors.edit', $author->id));
    }

    public function destroy(Author $author)
    {
        if ($author->delete()) {
            Session::flash('response', array('type' => 'success', 'message' => 'Author deleted successfully!'));
        } else {
            Session::flash('response', array('type' => 'error', 'message' => 'Something Went wrong!'));
        }
        return redirect(route('admin.authors.index'));
    }

    private function validateRequest()
    {
        return request()->validate([
            'name' => 'required|min:3',
            'bio' => 'sometimes',
            'status' => 'required',
            'image' => 'sometimes|file|image|max:5000',
        ]);
    }

    private function checkSlug($slug)
    {
        $all_slugs = Author::select('slug')->where('slug', 'like', $slug . '%')->get();

        if (!$all_slugs->contains('slug', $slug)) {
            return $slug;
        }
        $i = 1;
        while ($i++) {
            $new_slug = $slug . '-' . $i;
            if (!$all_slugs->contains('slug', $new_slug)) {
                return $new_slug;
            }
        }
    }

    private function createSlug($title)
    {
        $slug_text = Str::words($title, 15, '');
        return $this->checkSlug(mb_strtolower(preg_replace('/[ ,_\/@=]+/', '-', trim($slug_text)), 'UTF-8'));
    }

    private function uploadImage($image)
    {
        $timestemp = time();
        $image_name = $timestemp . '.' . $image->getClientOriginalExtension();
        $path = public_path('storage/uploads/profile_picture/author/') . 'image_' . $image_name;
        Image::make($image)->fit(400, 400)->save($path);
        return $image_name;
    }
}
