<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use App\Models\PostRequest;

class PostRequestsController extends Controller
{
    public function index()
    {
        $data = array(
            "post_requests" => PostRequest::paginate(500),
        );
        return view("admin.post_requests.index", $data);
    }

    public function show(PostRequest $post_request)
    {
        $data = array(
            "post_request" => $post_request,
        );
        return view("admin.post_requests.show", $data);
    }
}