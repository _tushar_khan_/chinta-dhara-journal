<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $data = array(
            "total_post" => Post::count(),
            "published_post" => Post::where('status', 1)->count(),
        );
        return view("admin.dashboard", $data);
    }
}
