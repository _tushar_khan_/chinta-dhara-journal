<?php
Route::get('/user', function () {
    return Auth()->user();
});

// Home Routes
Route::prefix('/')->namespace('Web\Home')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('articles', 'ArticleController@index')->name('articles.index');
    Route::get('articles/details/{article}', 'ArticleController@show')->where('article', '[0-9]+')->name('articles.show');
    Route::get('articles/details/{slug}', 'ArticleController@showBySlug')->name('articles.show-by-slug');
    Route::get('categories/{category}', 'CategoryController@show')->name('categories.show');
    Route::get('videos', 'VideoController@index')->name('videos.index');
    Route::get('videos/details/{video}', 'VideoController@show')->name('videos.show');
    Route::get('podcasts', 'PodcastController@index')->name('podcasts.index');
    Route::post('comments', 'CommentController@store')->name('comments.store');
    Route::post('comments/{comment}/reply', 'CommentController@reply_store')->name('comments.reply.store');
    Route::get('authors', 'AuthorController@index')->name('authors.index');
    Route::get('authors/{author}', 'AuthorController@show')->name('authors.show');
    Route::get('translators', 'TranslatorController@index')->name('translators.index');
    Route::get('translators/{translator}', 'TranslatorController@show')->name('translators.show');
    Route::get('socials', 'HomeController@socials')->name('socials');
    Route::get('post_box', 'HomeController@post_box')->name('post_box');
    Route::post('post_box', 'HomeController@post_box_store')->name('post_box.store');
    Route::get('search', 'SearchController@index')->name('search.index');
    Route::get('search/result', 'SearchController@search')->name('search.result');
});

// Admin Routes
Route::prefix('admin')->namespace('Web\Admin')->group(function () {
    // Admin Auth Routes
    Route::prefix('auth')->namespace('Auth')->group(function () {
        Route::get('/', 'LoginController@showLoginForm');
        Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
        Route::post('/login', 'LoginController@login');
        Route::post('/logout', 'LoginController@logout')->name('admin.logout');

        Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.request');
        Route::post('/password/reset', 'ResetPasswordController@reset')->name('admin.password.email');
        Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.reset');
        Route::get('/password/reset/{token}', 'ResetPasswordController@showResetForm');
    });

    Route::get('/login', function () {
        return redirect('admin/auth/login');
    });
    Route::get('/', function () {
        return redirect('admin/dashboard');
    });

    // Admin Dashboard Routes
    Route::prefix('/')->middleware('auth')->group(function () {
        Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
        Route::post('articles/uploadimage', 'ArticleController@uploadimage');
        Route::resource('articles', 'ArticleController', ['as' => 'admin']);
        Route::resource('authors', 'AuthorController', ['as' => 'admin']);
        Route::resource('translators', 'TranslatorController', ['as' => 'admin']);
        Route::resource('categories', 'CategoryController', ['as' => 'admin']);
        Route::resource('tags', 'TagController', ['as' => 'admin']);
        Route::get('post_requests', 'PostRequestsController@index')->name('admin.post_requests.index');
        Route::get('post_requests/{post_request}', 'PostRequestsController@show')->name('admin.post_requests.show');
    });
});