@extends('layouts.admin')
@section('title', 'Dashboard')

@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{url('#')}}">Add</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{url('#')}}">Add</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection

@section('content')
<div class="row mb-0">
    <div class="col s12 m6 l3">
        <div class="card counting-card blue lighten-5 primary-text">
            <div class="col s7 m7">
                <i class="material-icons background-round">description</i>
                <p class="counting-card-title">Posts</p>
            </div>
            <div class="col s5 m5 right-align">
                <h5 class="counting-card-value">{{ $published_post }}</h5>
                <p class="counting-card-value-title">Published</p>
                <p class="counting-card-second-value">
                    Total: <span>{{ $total_post }}</span>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
