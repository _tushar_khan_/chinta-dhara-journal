@extends('layouts.auth')

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-10 col-lg-5 mx-auto">
        <div class="card auth-form">
            <div class="from-title">{{ __('চিন্তাধারায় একাউন্ট করুন') }}</div>

            <div class="social-login" style="display: none">
                <div class="form-group">
                    <a href="#" class="button button-block facebook">
                        <span>ফেসবুক একাউন্ট দিয়ে লগইন করুন</span>
                    </a>
                </div>
                <div class="">
                    <a href="#" class="button button-block google">
                        <span>গুগল একাউন্ট দিয়ে লগইন করুন</span>
                    </a>
                </div>
            </div>
            <div class="devider" style="display: none">
                <span>অথবা</span>
            </div>
            <form method="POST" action="{{ route('writer.register') }}">
                @csrf
                <div class="form-group">
                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}"
                        placeholder="{{ __('নাম') }}" required autocomplete="email" autofocus />

                    @error('name')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group">
                    <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}"
                        placeholder="{{ __('ইমেইল') }}" required autocomplete="email" autofocus />

                    @error('email')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control"
                        placeholder="{{ __('পাসওয়ার্ড') }}" required autocomplete="new-password" />

                    @error('password')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirmation" id="password-confirm" class="form-control"
                        placeholder="{{ __('নিশ্চিতকরণ পাসওয়ার্ড') }}" required autocomplete="new-password" />

                    @error('password-confirm')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <button type="submit" class="button button-primary  button-block">
                    {{ __('একাউন্ট করুন') }}
                </button>
            </form>
            <a href="{{route('writer.login')}}" class="ml-auto mt-4">লগ-ইন করুন</a>
        </div>
    </div>
</div>
@endsection
