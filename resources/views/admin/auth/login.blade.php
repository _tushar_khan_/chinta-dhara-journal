@extends('layouts.auth')

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-10 col-lg-5 mx-auto">
        <div class="card auth-form">
            <div class="from-title">{{ __('চিন্তাধারা - এডমিন') }}</div>
            <form method="POST" action="{{ route('admin.login') }}">
                @csrf
                <div class="form-group">
                    <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}"
                        placeholder="{{ __('ইমেইল') }}" required autocomplete="email" autofocus>
                    @error('email')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="password" class="form-control"
                        placeholder="{{ __('পাসওয়ার্ড') }}" required autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>
                <button type="submit" class="button button-primary  button-block">
                    {{ __('লগ ইন') }}
                </button>
            </form>
            <a href="{{route('admin.password.email')}}" class="ml-auto mt-4">
                পাসওয়ার্ড ভুলে গেছেন?
            </a>

        </div>
    </div>
</div>
@endsection
