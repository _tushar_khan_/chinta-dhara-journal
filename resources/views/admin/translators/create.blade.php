@extends('layouts.admin')
@section('title', 'Add Translator')

@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{ url('admin/translators/create') }}">Add Translator</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{ url('admin/translators/create') }}">Add Translator</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card panel">
    <div class="panel-header">
        <span class="panel-title">Add New Translator</span>
        <div class="actions">
            <a href="{{ url('admin/translators/create') }}" class="btn-flat">
                <i class="material-icons">add</i>
            </a>
            <a href="{{ url('admin/translators') }}" class="btn-flat">
                <i class="material-icons">close</i>
            </a>
        </div>
    </div>
    <div class="card-content">
        <div class="row">
            <form action="{{ route('admin.translators.store') }}" method="POST" enctype="multipart/form-data">
                @include('admin.translators.form')
                <div class="input-field col s12">
                    <button type="submit" name="action" class="btn custom primary waves-effect waves-light">
                        <i class="material-icons left">add</i> Add Translator
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
