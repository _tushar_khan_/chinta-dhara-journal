@extends('layouts.admin')
@section('title', 'Edit Category')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{url('admin/categories/create')}}">Add Category</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{url('admin/categories/create')}}">Add Category</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card panel">
    <div class="panel-header">
        <span class="panel-title">Edit Category</span>
        <div class="actions">
            <a href="{{ url('admin/categories') }}" class="btn-flat">
                <i class="material-icons">close</i>
            </a>
        </div>
    </div>
    <div class="card-content">
        <div class="row">
            <form action="{{ route('admin.categories.update', $category['id']) }}" method="POST"
                enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                @csrf
                <div class="col s9 pr-2">
                    <div class="row">
                        <div class="input-field outline col s12">
                            <input name="name" id="name" type="text" class="@error('name') invalid @enderror"
                                value="{{ $category->name }}" autofocus>

                            @error('name')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="col s9 pr-2">
                    <div class="row">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control" name="status" id="status" required>
                                <option value="1" @if($category->status == 1) {{ 'selected' }} @endif>Active</option>
                                <option value="0" @if($category->status == 0) {{ 'selected' }} @endif>Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="input-field col s12">
                    <button class="btn custom teal waves-effect waves-light" type="submit" name="action">
                        <i class="material-icons left">add</i> Update Category
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
