@extends('layouts.admin')
@section('title', 'Categories')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{ url('admin/categories/create') }}">Add Category</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{ url('admin/categories/create') }}">Add Category</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card material-data-table">
    <div class="table-header">
        <span class="table-title">All Categories</span>
        <div class="actions">
            <a href="{{ route('admin.categories.create') }}" class="btn-flat">
                <i class="material-icons">add</i>
            </a>
            <a href="JavaScript:Void(0);" class="search-toggle waves-effect btn-flat">
                <i class="material-icons">search</i>
            </a>
        </div>
    </div>
    <table class="datatable responsive-table">
        <thead>
            <tr>
                <th class="hide">{{ _('#') }}</th>
                <th>{{ _('Name') }}</th>
                <th>{{ _('Status') }}</th>
                <th>{{ _('Created At') }}</th>
                <th>{{ _('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $index => $category)
            <tr>
                <td class="hide">{{ $index + 1 }}</td>
                <td>{{ $category->name }}</td>
                <td>{{ $category->status }}</td>
                <td>{{date('d M, Y', strtotime($category->created_at))}}</td>
                <td>
                    <a href="{{ route('categories.show', $category->id) }}" target="_blank"
                        class="btn-floating btn-flat btn-small waves-effect waves-light teal mr-3">
                        <i class="material-icons">visibility</i></a>
                    <a href="{{ route('admin.categories.edit', $category->id) }}"
                        class="btn-floating btn-flat btn-small waves-effect waves-light teal mr-3">
                        <i class="material-icons">edit</i></a>
                    <form action="{{ route('admin.categories.destroy', $category->id) }}" method="POST"
                        style="display: inline-block;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn-floating btn-flat btn-small waves-effect waves-light red"
                            onclick="return(confirm('Are you sure to delete?'))"><i
                                class="material-icons">delete</i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
