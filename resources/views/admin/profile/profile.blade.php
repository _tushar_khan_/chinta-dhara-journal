@extends('layouts.admin')
@section('title', 'Admin Profile')
@section('content')
<div class="card profile-page-header">
    <figure class="card-profile-image">
        <img src="{{asset('images/avatar.png')}}" alt="profile image" class="circle responsive-img activator">
    </figure>
    <div class="card-content">
        <div class="row">
            <div class="col s12 m3 offset-m2">
                <h4 class="card-title grey-text text-darken-4">{{$profile->first_name}}
                    {{$profile->last_name}}</h4>
                <p class="medium-small grey-text">Customer</p>
            </div>
            <div class="col s12 m2 center-align">
                <h4 class="card-title grey-text text-darken-4">Package</h4>
                <p class="medium-small grey-text">A</p>
            </div>
            <div class="col s12 m2 center-align">
                <h4 class="card-title grey-text text-darken-4">Area</h4>
                <p class="medium-small grey-text">B</p>
            </div>
            <div class="col s12 m2 center-align">
                <h4 class="card-title grey-text text-darken-4">Total Bill</h4>
                <p class="medium-small grey-text">Busness Profit</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col s12 m6">
        <div class="card panel">A</div>
    </div>
</div>
@endsection
