@extends('layouts.admin')
@section('title', 'Add Article')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{url('admin/articles')}}">All Article</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{url('admin/articles')}}">All Article</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card panel">
    <div class="panel-header">
        <span class="panel-title">Add New Article</span>
        <div class="actions">
            <a href="{{ url('admin/articles') }}" class="btn-flat">
                <i class="material-icons">close</i>
            </a>
        </div>
    </div>
    <div class="card-content">
        <div class="row">
            <form id="create-post" action="{{ route('admin.articles.store')}}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <div class="col s9 pr-2">
                    <div class="row">
                        <div class="input-field outline col s12">
                            <input name="title" id="title" type="text" value="{{ old('title') }}"
                                class="@error('title') invalid @enderror" required autofocus>
                            <label for="title">{{ __('Title') }}</label>
                            @error('title')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <input name="slug" id="slug" type="text" value="{{ old('slug') }}"
                                class="@error('slug') invalid @enderror">
                            <label for="slug">{{ __('Slug URL') }}</label>
                            @error('slug')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <div id="quill-editor"></div>
                            <input type="hidden" name="body">
                            @error('body')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col s3">
                    <div class="row">
                        <div class="input-field outline col s12">
                            <select class="material-select" id="categories" name="categories">
                                @if(isset($categories) && count($categories))
                                <option disabled selected>{{ __('ক্যাটাগরি সিলেক্ট করুন') }}</option>
                                @foreach( $categories as $key => $category )
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                                @else
                                <option disabled selected>{{ __('কোন ক্যাটাগরি নেই') }}</option>
                                @endif
                            </select>
                            <label for="categories">{{ __('ক্যাটাগরি') }}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <select class="material-select" id="authors" name="authors">
                                @if(isset($authors) && count($authors))
                                <option disabled selected>{{ __('লেখক সিলেক্ট করুন') }}</option>
                                @foreach( $authors as $key => $author )
                                <option value="{{ $author->id }}">{{ $author->name }}</option>
                                @endforeach
                                @else
                                <option disabled selected>{{ __('কোন লেখক নেই') }}</option>
                                @endif
                            </select>
                            <label for="authors">{{ __('লেখক') }}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <select class="material-select" id="translators" name="translators">
                                @if(isset($translators) && count($translators))
                                <option disabled selected>{{ __('অনুবাদক সিলেক্ট করুন') }}</option>
                                @foreach( $translators as $key => $translator )
                                <option value="{{ $translator->id }}">{{ $translator->name }}</option>
                                @endforeach
                                @else
                                <option disabled selected>{{ __('কোন অনুবাদক নেই') }}</option>
                                @endif
                            </select>
                            <label for="translators">{{ __('অনুবাদক') }}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <div class="file-field">
                                <div class="thumb-preview">
                                    <img src="" alt="" id="preview" style="height: auto;
                                    width: 100%;">
                                </div>
                                <div class="btn-flat custom teal white-text btn-block center">
                                    <span>নির্বাচিত ছবি</span>
                                    <input type="file" name="thumbnail" id="thumbnail"
                                        onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="input-field col s12">
                    <button type="submit" class="btn custom teal waves-effect waves-light" name="action">
                        <i class="material-icons left">add</i> {{ __('পোস্ট করুন') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Main Quill library -->
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="https://cdn.quilljs.com/1.3.6/quill.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/quill-image-resize-module@3.0.0/image-resize.min.js"></script>

<!-- Theme included stylesheets -->
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="https://cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
<script>
    var quill = new Quill('#quill-editor', {
        theme: 'snow',
        modules: {
            toolbar: {
                container: [
                    [{
                        header: [1, 2, 3, 4, 5, 6, false]
                    }],
                    ['bold', 'italic', 'underline', 'strike'],
                    ['link', 'blockquote'],
                    ['image', 'video'],
                    [{
                        'align': []
                    }],
                    [{
                        'list': 'ordered'
                    }, {
                        'list': 'bullet'
                    }],
                    [{
                        'indent': '-1'
                    }, {
                        'indent': '+1'
                    }],
                    [{
                        'direction': 'rtl'
                    }],
                    [{
                        'color': []
                    }, {
                        'background': []
                    }]
                ],
                handlers: {
                    image: imageHandler
                }
            },

            imageResize: {
                modules: ['Resize', 'DisplaySize', 'Toolbar']
            }
        },
    });

    function imageHandler() {
        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*')
        input.click();

        // Listen upload local image and save to server
        input.onchange = () => {
            const file = input.files[0];
            // file type is only image.
            if (/^image\//.test(file.type)) {
                const fd = new FormData();
                fd.append('image', file);

                const xhr = new XMLHttpRequest();
                xhr.open("POST", "{{ url('admin/articles/uploadimage?_token=' . csrf_token()) }}", true);
                xhr.onload = () => {
                    if (xhr.status === 200) {
                        // this is callback data: url
                        const url = xhr.responseText;
                        const range = quill.getSelection();
                        quill.insertEmbed(range.index, 'image', url);
                    } else {
                        alert('Image upload failed');
                    }
                };
                xhr.send(fd);
            } else {
                console.warn('You could only upload images.');
            }
        };
    }

</script>
<script>
    var form = document.getElementById("create-post");

    form.onsubmit = function () {
        console.log('aaa');
        return false;
    }

    form.onsubmit = function () {
        var body = document.querySelector('input[name=body]');
        body.value = JSON.stringify(quill.getContents());
        return true;
    }

</script>

<style>
    .ql-container strong {
        font-weight: bold;
    }

</style>
@endsection
