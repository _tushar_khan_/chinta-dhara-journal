@extends('layouts.admin')
@section('title', 'Articles')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{url('admin/articles/create')}}">Add Article</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{url('admin/articles/create')}}">Add Article</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card panel">
    <div class="panel-header">
        <span class="panel-title">Edit Article</span>
        <div class="actions">
            <a href="{{ url('admin/articles') }}" class="btn-flat">
                <i class="material-icons">close</i>
            </a>
        </div>
    </div>
    <div class="card-content">
        <div class="row">
            @if(isset($article))
            <form action="{{ route('admin.articles.update', $article->id) }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                @method("PUT")
                <div class="col s9 pr-2">
                    <div class="row">
                        <div class="input-field outline col s12">
                            <input name="title" id="title" type="text" value="{{ $article->title }}"
                                class="@error('title') invalid @enderror" required autofocus>
                            <label for="title">{{ __('Title') }}</label>
                            @error('title')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <input name="slug" id="slug" type="text" value="{{ $article->slug }}"
                                class="@error('slug') invalid @enderror">
                            <label for="slug">{{ __('Slug URL') }}</label>
                            @error('slug')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <textarea name="body" id="body" class="ck-editor"
                                style="min-height: 100px">{{ $article->body }}</textarea>
                            @error('body')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col s3">
                    <div class="row">
                        <div class="input-field outline col s12">
                            <select class="material-select" id="categories" name="categories">
                                @if(isset($categories) && count($categories))
                                <option disabled selected>{{ __('ক্যাটাগরি সিলেক্ট করুন') }}</option>
                                @foreach( $categories as $key => $category )
                                <option value="{{ $category->id }}" @if(count($article->categories) && $category->id ==
                                    $article->categories[0]->id){{ 'selected' }} @endif>{{ $category->name }}</option>
                                @endforeach
                                @else
                                <option disabled selected>{{ __('কোন ক্যাটাগরি নেই') }}</option>
                                @endif
                            </select>
                            <label for="categories">{{ __('ক্যাটাগরি') }}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <select class="material-select" id="authors" name="authors">
                                @if(isset($authors) && count($authors))
                                <option disabled selected>{{ __('লেখক সিলেক্ট করুন') }}</option>
                                @foreach( $authors as $key => $author )
                                <option value="{{ $author->id }}" @if(count($article->authors) && $author->id ==
                                    $article->authors[0]->id){{ 'selected' }} @endif>{{ $author->name }}</option>
                                @endforeach
                                @else
                                <option disabled selected>{{ __('কোন লেখক নেই') }}</option>
                                @endif
                            </select>
                            <label for="authors">{{ __('লেখক') }}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <select class="material-select" id="translators" name="translators">
                                @if(isset($translators) && count($translators))
                                <option disabled selected>{{ __('অনুবাদক সিলেক্ট করুন') }}</option>
                                @foreach( $translators as $key => $translator )
                                <option value="{{ $translator->id }}" @if(count($article->translators) &&
                                    $translator->id ==
                                    $article->translators[0]->id){{ 'selected' }} @endif>{{ $translator->name }}
                                </option>
                                @endforeach
                                @else
                                <option disabled selected>{{ __('কোন অনুবাদক নেই') }}</option>
                                @endif
                            </select>
                            <label for="translators">{{ __('অনুবাদক') }}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <div class="file-field">
                                <div class="thumb-preview">
                                    <img src="{{ $article->thumbnail ? url('storage/uploads/post/thumbnail/thumb_' . $article->thumbnail) : asset('images/no-thumb.jpg') }}"
                                        alt="" id="preview" style="height: auto;
                                    width: 100%;">
                                </div>
                                <div class="btn-flat custom teal white-text btn-block center">
                                    <span>নির্বাচিত ছবি</span>
                                    <input type="file" name="thumbnail" id="thumbnail"
                                        onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="input-field col s12">
                    <button type="submit" class="btn custom teal waves-effect waves-light" name="action">
                        <i class="material-icons left">add</i> {{ __('পোস্ট এডিট করুন') }}
                    </button>
                </div>
            </form>
            @else
            <p>{{ __('কোন পোস্ট পাওয়া যায়নি') }}</p>
            @endif
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.6.2/standard-all/ckeditor.js"></script>
<script>
    CKEDITOR.replace('body', {
        height: 300,
        filebrowserUploadUrl: "{{ url('admin/articles/uploadimage?_token=' . csrf_token()) }}"
    });

</script>
<style>
    .ck-editor__editable {
        min-height: 300px;
    }

</style>
@endsection
