@extends('layouts.admin')
@section('title', 'Articles')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{url('admin/articles/create')}}">Add Article</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{url('admin/articles/create')}}">Add Article</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card material-data-table">
    <div class="table-header">
        <span class="table-title">All Articles</span>
        <div class="actions">
            <a href="{{url('admin/articles/create')}}" class="btn-flat">
                <i class="material-icons">add</i>
            </a>
            <a href="JavaScript:Void(0);" class="search-toggle waves-effect btn-flat">
                <i class="material-icons">search</i>
            </a>
        </div>
    </div>
    <table class="datatable responsive-table">
        <thead>
            <tr>
                <th class="hide">{{ __('#') }}</th>
                <th>{{ __('Title') }}</th>
                <th>{{ __('Category') }}</th>
                <th>{{ __('Author') }}</th>
                <th>{{ __('Translator') }}</th>
                <th>{{ __('Status') }}</th>
                <th>{{ __('Created At') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($articles as $index => $article)
            <tr>
                <td class="hide">{{ $article->index }}</td>
                <td>{{ $article->title }}</td>
                <td>
                    @if(isset($article->categories[0]->name))
                    {{ $article->categories[0]->name }}
                    @else
                    {{ __('-') }}
                    @endif
                </td>
                <td>
                    @if(isset($article->authors[0]->name))
                    {{ $article->authors[0]->name }}
                    @else
                    {{ __('-') }}
                    @endif
                </td>
                <td>
                    @if(isset($article->translators[0]->name))
                    {{ $article->translators[0]->name }}
                    @else
                    {{ __('-') }}
                    @endif
                </td>
                <td>{{ $article->status }}</td>
                <td>
                    {{date('d-m-Y', strtotime($article->created_at))}}
                </td>
                <td>
                    <a href="{{ route('articles.show', $article->id) }}" target="_blank"
                        class="btn-floating btn-flat btn-small waves-effect waves-light primary mr-3">
                        <i class="material-icons">visibility</i>
                    </a>
                    <a href="{{ route('admin.articles.edit', $article->id) }}"
                        class="btn-floating btn-flat btn-small waves-effect waves-light primary mr-3">
                        <i class="material-icons">edit</i>
                    </a>
                    <form action="{{ route('admin.articles.destroy', $article->id) }}" method="POST"
                        style="display: inline-block;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn-floating btn-flat btn-small waves-effect waves-light red"
                            onclick="return(confirm('Are you sure to delete?'))">
                            <i class="material-icons">delete</i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
