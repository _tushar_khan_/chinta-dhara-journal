@extends('layouts.admin')
@section('title', 'Add Article')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{url('admin/articles')}}">All Article</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{url('admin/articles')}}">All Article</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card panel">
    <div class="panel-header">
        <span class="panel-title">Add New Article</span>
        <div class="actions">
            <a href="{{ url('admin/articles') }}" class="btn-flat">
                <i class="material-icons">close</i>
            </a>
        </div>
    </div>
    <div class="card-content">
        <div class="row">
            <form action="{{ route('admin.articles.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col s9 pr-2">
                    <div class="row">
                        <div class="input-field outline col s12">
                            <input name="title" id="title" type="text" value="{{ old('title') }}"
                                class="@error('title') invalid @enderror" required autofocus>
                            <label for="title">{{ __('Title') }}</label>
                            @error('title')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <input name="slug" id="slug" type="text" value="{{ old('slug') }}"
                                class="@error('slug') invalid @enderror">
                            <label for="slug">{{ __('Slug URL') }}</label>
                            @error('slug')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <textarea name="body" id="body" class="ck-editor"
                                style="min-height: 100px">{{ old('body') }}</textarea>
                            @error('body')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col s3">
                    <div class="row">
                        <div class="input-field outline col s12">
                            <select id="categories" name="categories">
                                @if(isset($categories) && count($categories))
                                <option disabled selected>{{ __('ক্যাটাগরি সিলেক্ট করুন') }}</option>
                                @foreach( $categories as $key => $category )
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                                @else
                                <option disabled selected>{{ __('কোন ক্যাটাগরি নেই') }}</option>
                                @endif
                            </select>
                            <label for="categories">{{ __('ক্যাটাগরি') }}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <select id="authors" name="authors">
                                @if(isset($authors) && count($authors))
                                <option disabled selected>{{ __('লেখক সিলেক্ট করুন') }}</option>
                                @foreach( $authors as $key => $author )
                                <option value="{{ $author->id }}">{{ $author->name }}</option>
                                @endforeach
                                @else
                                <option disabled selected>{{ __('কোন লেখক নেই') }}</option>
                                @endif
                            </select>
                            <label for="authors">{{ __('লেখক') }}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field outline col s12">
                            <select id="translators" name="translators">
                                @if(isset($translators) && count($translators))
                                <option disabled selected>{{ __('অনুবাদক সিলেক্ট করুন') }}</option>
                                @foreach( $translators as $key => $translator )
                                <option value="{{ $translator->id }}">{{ $translator->name }}</option>
                                @endforeach
                                @else
                                <option disabled selected>{{ __('কোন অনুবাদক নেই') }}</option>
                                @endif
                            </select>
                            <label for="translators">{{ __('অনুবাদক') }}</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <div class="file-field">
                                <div class="thumb-preview">
                                    <img src="" alt="" id="preview" style="height: auto;
                                    width: 100%;">
                                </div>
                                <div class="btn-flat custom teal white-text btn-block center">
                                    <span>নির্বাচিত ছবি</span>
                                    <input type="file" name="thumbnail" id="thumbnail"
                                        onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="input-field col s12">
                    <button type="submit" class="btn custom teal waves-effect waves-light" name="action">
                        <i class="material-icons left">add</i> {{ __('পোস্ট করুন') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.6.2/standard-all/ckeditor.js"></script>
<script>
    CKEDITOR.replace('body', {
        height: 300,
        //filebrowserUploadUrl: "{{ url('admin/articles/uploadimage') }}"
        filebrowserUploadUrl: "{{ url('admin/articles/uploadimage?_token=' . csrf_token()) }}"
    });

</script>
<style>
    .ck-editor__editable {
        min-height: 300px;
    }

</style>
@endsection
