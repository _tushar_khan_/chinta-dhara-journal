@extends('layouts.admin')
@section('title', 'Post Requests')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{url('admin/articles/create')}}">Add Article</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{url('admin/articles/create')}}">Add Article</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card material-data-table">
    <div class="table-header">
        <span class="table-title">All Post Requests</span>
        <div class="actions">
            <a href="JavaScript:Void(0);" class="search-toggle waves-effect btn-flat">
                <i class="material-icons">search</i>
            </a>
        </div>
    </div>
    <table class="datatable responsive-table">
        <thead>
            <tr>
                <th class="hide">{{ __('#') }}</th>
                <th>{{ __('Title') }}</th>
                <th>{{ __('Author') }}</th>
                <th>{{ __('Phone') }}</th>
                <th>{{ __('Email') }}</th>
                <th>{{ __('Status') }}</th>
                <th>{{ __('Created At') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($post_requests as $index => $post_request)
            <tr>
                <td class="hide">{{ $post_request->index }}</td>
                <td>{{ $post_request->title }}</td>
                <td>{{ $post_request->author_name }}</td>
                <td>{{ $post_request->author_phone }}</td>
                <td>{{ $post_request->author_email }}</td>
                <td>{{ $post_request->status }}</td>
                <td>
                    {{ date('d-m-Y', strtotime($post_request->created_at)) }}
                </td>
                <td>
                    <a href="{{ route('admin.post_requests.show', $post_request->id) }}"
                        class="btn-floating btn-flat btn-small waves-effect waves-light primary mr-3">
                        <i class="material-icons">visibility</i>
                    </a>
                    <form action="{{ route('admin.articles.destroy', $post_request->id) }}" method="POST"
                        style="display: inline-block;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn-floating btn-flat btn-small waves-effect waves-light red"
                            onclick="return(confirm('Are you sure to delete?'))">
                            <i class="material-icons">delete</i>
                        </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
