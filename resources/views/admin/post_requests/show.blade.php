@extends('layouts.admin')
@section('title', 'Post Request')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{url('admin/articles/create')}}">Add Article</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{url('admin/articles/create')}}">Add Article</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card panel">
    <div class="panel-header">
        <span class="panel-title">Post details</span>
    </div>
    <div class="card-content">
        <h5>{{ $post_request->title }}</h5>
        <p>{{ __('লেখক : ') . $post_request->author_name }}</p>
        <p>{{ $post_request->author_phone ? __('ফোন : ') . $post_request->author_phone . ', ' : '' }}
            {{ $post_request->author_email ? __('ইমেইল : ') . $post_request->author_email . ', ' : '' }}
            {{ __('পাঠিয়েছে : ') . Bengali::bn_date_time(date('d F Y', strtotime($post_request->created_at))) }}
        </p>
        <hr>
        <p id="body">{!! $post_request->body !!}</p>
        <button class="btn" onclick="copyText()">Copy</button>

    </div>
</div>

<script>
    function copyText() {
        var range = document.createRange();
        range.selectNode(document.getElementById("body"));
        window.getSelection().removeAllRanges(); // clear current selection
        window.getSelection().addRange(range); // to select text
        document.execCommand("copy");
        window.getSelection().removeAllRanges(); // to deselect
        alert("Copied to the clipbord!");
    }

</script>
@endsection
