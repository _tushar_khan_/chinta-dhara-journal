@extends('layouts.admin')
@section('title', 'Edit Author')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{url('admin/authors/create')}}">Add Author</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{url('admin/authors/create')}}">Add Author</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card panel">
    <div class="panel-header">
        <span class="panel-title">Edit Author</span>
        <div class="actions">
            <a href="{{url('admin/authors/create')}}" class="btn-flat">
                <i class="material-icons">add</i>
            </a>
            <a href="{{url('admin/authors')}}" class="btn-flat">
                <i class="material-icons">close</i>
            </a>
        </div>
    </div>
    <div class="card-content">
        <div class="row">
            <form action="{{ route('admin.authors.update', $author->id) }}" method="POST" enctype="multipart/form-data">
                @method('PATCH')
                @include('admin.authors.form')
                <div class="input-field col s12">
                    <button type="submit" name="action" class="btn custom teal waves-effect waves-light">
                        <i class="material-icons left">done</i> Update Author
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
