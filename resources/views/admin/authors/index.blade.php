@extends('layouts.admin')
@section('title', 'Authors')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{ url('admin/authors/create') }}">Add Author</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{ url('admin/authors/create') }}">Add Author</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card material-data-table">
    <div class="table-header">
        <span class="table-title">All Authors</span>
        <div class="actions">
            <a href="{{ url('admin/authors/create') }}" class="btn-flat">
                <i class="material-icons">add</i>
            </a>
            <a href="JavaScript:Void(0);" class="search-toggle waves-effect btn-flat">
                <i class="material-icons">search</i>
            </a>
        </div>
    </div>
    <table class="datatable responsive-table">
        <thead>
            <tr>
                <th>{{ __('Name') }}</th>
                <th>{{ __('Status') }}</th>
                <th>{{ __('Created At') }}</th>
                <th>{{ __('Action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($authors as $index => $author)
            <tr>
                <td>{{ $author->name }}</td>
                <td>{{ $author->status }}</td>
                <td>{{date('d-m-Y', strtotime($author->created_at))}}</td>
                <td>
                    <a href="{{ route('admin.authors.edit', $author->id) }}"
                        class="btn-floating btn-flat btn-small waves-effect waves-light teal mr-3"><i
                            class="material-icons">edit</i></a>
                    <form action="{{ route('admin.authors.destroy', $author->id) }}" method="POST"
                        style="display: inline-block;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn-floating btn-flat btn-small waves-effect waves-light red"
                            onclick="return(confirm('Are you sure to delete?'))"><i
                                class="material-icons">delete</i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
