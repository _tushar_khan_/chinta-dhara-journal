@csrf
<div class="col m9 s12">
    <div class="row mb-0">
        <div class="input-field outline col s12">
            <input name="name" id="name" type="text" value="{{ old('name') ?? $author->name }}" required autofocus />
            <label for="name">{{ __('Author Name') }}</label>
            @error('name')
            <span class="helper-text red-text">{{
                $message
            }}</span>
            @enderror
        </div>
    </div>
    <div class="row mb-0">
        <div class="input-field outline col s12">
            <input name="slug" id="slug" type="text" value="{{ old('slug') ?? $author->slug }}" />
            <label for="slug">{{ __('Slug') }}</label>
            @error('slug')
            <span class="helper-text red-text">{{
                $message
            }}</span>
            @enderror
        </div>
    </div>
    <div class="row mb-0">
        <div class="input-field outline col s12">
            <textarea name="bio" id="bio" class="materialize-textarea">{{ old('bio') ?? $author->bio }}</textarea>
            <label for="bio">{{ __('Author Bio') }}</label>
            @error('bio')
            <span class="helper-text red-text">{{ $message }}</span>
            @enderror
        </div>
    </div>
    <div class="row">
        <div class="input-field outline col s12">
            <select class="material-select" name="status" id="status">
                <option disabled>Select Author Status</option>
                @foreach($author->statusOptions() as $statusOptionKey => $statusOptionValue)
                <option value="{{ $statusOptionKey }}" {{ $author->status == $statusOptionValue ? 'selected' : '' }}>
                    {{ $statusOptionValue }}</option>
                @endforeach
            </select>
            <label for="translators">{{ __('Status') }}</label>
            @error('status')
            <span class="helper-text red-text">{{ $message }}</span>
            @enderror
        </div>
    </div>
</div>
<div class="col m3 s12">
    <div class="row">
        <div class="input-field col s12">
            <div class="file-field">
                <div class="thumb-preview">
                    <img src=" {{ $author->image ? url('storage/uploads/profile_picture/author/image_') . $author->image : asset('images/writer-image.jpg') }}"
                        alt="" id="preview" style="height: auto;
                        width: 100%;" />
                </div>
                <div class="btn-flat custom primary white-text btn-block center">
                    <span>নির্বাচিত ছবি</span>
                    <input type="file" name="image" id="image"
                        onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])" />
                </div>
            </div>
            @error('image')
            <span class="helper-text red-text">{{ $message }}</span>
            @enderror
        </div>
    </div>
</div>
