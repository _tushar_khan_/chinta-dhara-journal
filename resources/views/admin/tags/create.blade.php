@extends('layouts.admin')
@section('title', 'Add Tag')
@section('second-nav-bar')
<ul class="right hide-on-med-and-down">
    <li>
        <a href="{{url('admin/tags/create')}}">Add Tag</a>
    </li>
</ul>
<ul id="nav-mobile" class="sidenav">
    <li>
        <a href="{{url('admin/tags/create')}}">Add Tag</a>
    </li>
</ul>
<a href="#" data-target="nav-mobile" class="sidenav-trigger">
    <i class="material-icons">menu</i>
</a>
@endsection
@section('content')
<div class="card panel">
    <div class="panel-header">
        <span class="panel-title">Add New Tag</span>
        <div class="actions">
            <a href="{{ url('admin/tags') }}" class="btn-flat">
                <i class="material-icons">close</i>
            </a>
        </div>
    </div>
    <div class="card-content">
        <div class="row">
            <form action="{{ route('admin.tags.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col s9 pr-2">
                    <div class="row">
                        <div class="input-field outline col s12">
                            <input name="name" id="name" type="text" class="@error('name') invalid @enderror" required
                                autofocus>
                            <label for="name">{{ __('Tag Name') }}</label>
                            @error('name')
                            <span class="helper-text red-text">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="col s9 pr-2">
                    <div class="row">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control @error('status') is-invalid @enderror" name="status" id="status"
                                required>
                                <option value="1" selected>
                                    Active
                                </option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="input-field col s12">
                    <button class="btn custom teal waves-effect waves-light" type="submit" name="action">
                        <i class="material-icons left">add</i> Add Tag
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
