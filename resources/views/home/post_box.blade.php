@extends('layouts.app')

@section('content')
<section class="container mt-5">
    <div class="section-title mb-3">
        <h4 class="title">{{ __('লিখা পাঠান') }}</h4>
    </div>
    <div class="post-box">
        @if(Session::has('response'))
        <div class="alert alert-{{ Session::get('response.type') == 'success' ? 'success' : 'danger' }} mb-5">
            {{ Session::get('response.message') }}
        </div>
        @endif
        <form action=" {{ route('post_box.store') }}" method="POST">
            @csrf
            <div class="form-group mb-4">
                <label for="author_name" class="m-0">নাম (আবশ্যক)</label>
                <input type="text" name="author_name" id="author_name" value="{{ old('author_name') }}"
                    class="form-control {{ $errors->has('author_name') ? 'is-invalid' : '' }}" required
                    placeholder="আপনার নাম লিখুন">
                @if($errors->has('author_name'))
                <div class="invalid-feedback">
                    {{ $errors->first('author_name') }}
                </div>
                @endif
            </div>
            <div class="form-group mb-4">
                <label for="author_phone" class="m-0">ফোন নম্বর</label>
                <input type="text" name="author_phone" id="author_phone" value="{{ old('author_phone') }}"
                    class="form-control {{ $errors->has('author_phone') ? 'is-invalid' : '' }}"
                    placeholder="আপনার ফোন নম্বর লিখুন">
                @if($errors->has('author_phone'))
                <div class="invalid-feedback">
                    {{ $errors->first('author_phone') }}
                </div>
                @endif
            </div>
            <div class="form-group mb-4"> <label for="author_email" class="m-0">আপনার ইমেইল</label>
                <input type="text" name="author_email" id="author_email" value="{{ old('author_email') }}"
                    class="form-control {{ $errors->has('author_email') ? 'is-invalid' : '' }}"
                    placeholder="আপনার ইমেইল লিখুন">
                @if($errors->has('author_email'))
                <div class="invalid-feedback">
                    {{ $errors->first('author_email') }}
                </div>
                @endif
            </div>
            <div class="form-group mb-4">
                <label for="title" class="m-0">টাইটেল (আবশ্যক)</label>
                <input type="text" name="title" id="title" value="{{ old('title') }}"
                    class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" required
                    placeholder="পোষ্ট টাইটেল লিখুন">
                @if($errors->has('title'))
                <div class="invalid-feedback">
                    {{ $errors->first('title') }}
                </div>
                @endif
            </div>

            <div class="form-group mb-4">
                <label for="body" class="m-0">বিস্তারিত পোষ্ট (আবশ্যক)</label>
                <textarea name="body" id="body" class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}"
                    cols="30" rows="3" required placeholder="আপনার সম্পূর্ণ লিখা দিন"> {{ old('body') }}</textarea>
                @if($errors->has('body'))
                <div class="invalid-feedback">
                    {{ $errors->first('body') }}
                </div>
                @endif
            </div>
            <div class="form-group mb-4">
                <button type="submit" class="button button-primary">অনুরোধ পাঠান</button>
            </div>
        </form>
    </div>
</section>
@endsection
