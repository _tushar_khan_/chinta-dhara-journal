@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="mb-2">
        <h2 class="cat-page-title">ক্যাটাগরিঃ {{ $category->name}}</h2>
    </div>
    @if( isset($posts) && count($posts) > 0)
    <div class="home-recent-posts">
        <div class="row">
            @foreach( $posts as $post )
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="single-post">
                    <div class="card-post">
                        <a href="{{ route(strtolower($post->post_type) . 's.show-by-slug', $post->slug)}}">
                            <img src="@if($post->thumbnail != null) {{ url('storage/uploads/post/thumbnail/thumb_' . $post->thumbnail) }} @else {{ asset('images/no-thumb.jpg')}} @endif"
                                alt="{!! Str::words($post->title, 10,'...') !!}" class="post-thumb">
                        </a>
                        <div class="post-meta">
                            <span class="post-author">
                                @if(count($post->authors) > 0)
                                <a href="{{ route('authors.show', $post->authors[0]->id) }}">
                                    {{ $post->authors[0]->name }}
                                </a>
                                @elseif(count($post->translators) > 0)
                                <a href="{{ route('translators.show', $post->translators[0]->id) }}">
                                    {{ $post->translators[0]->name }}
                                </a>
                                @else
                                {{ __('চিন্তাধারা') }}
                                @endif
                            </span>
                            <span>{{  Bengali::bn_number($post->views) . __(' বার পঠিত') }}</span>

                        </div>
                        <a href="{{ route(strtolower($post->post_type) . 's.show-by-slug', $post->slug)}}"
                            class="post-title">
                            {!! Str::words($post->title, 10,'...') !!}
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @else
    <p>{{ __('এই ক্যাটাগরিতে এখনো পোস্ট করা হয়নি...') }}</p>
    @endif
</div>
@endsection
