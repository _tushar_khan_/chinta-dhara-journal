@extends('layouts.app')

@section('content')
<div class="container mt-5 pb-5">
    <section class="home-recent-posts">
        <div class="row">
            @if( isset($articles) && count($articles) )
            @foreach( $articles as $article )
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="single-post">
                    <div class="card-post">
                        <a href="{{ route(strtolower($article->post_type) . 's.show-by-slug', $article->slug) }}">
                            <img src="@if($article->thumbnail != null) {{ url('storage/uploads/post/thumbnail/thumb_' . $article->thumbnail) }} @else {{ asset('images/no-thumb.jpg')}} @endif"
                                alt="{!! Str::words($article->title, 10,'...') !!}" class="post-thumb">
                        </a>
                        <div class="post-meta">
                            <span class="post-author">
                                @if(count($article->authors) > 0)
                                <a href="{{ route('authors.show', $article->authors[0]->id) }}">
                                    {{ $article->authors[0]->name }}
                                </a>
                                @elseif(count($article->translators) > 0)
                                <a href="{{ route('translators.show', $article->translators[0]->id) }}">
                                    {{ $article->translators[0]->name }}
                                </a>
                                @else
                                {{ __('চিন্তাধারা') }}
                                @endif
                            </span>
                            <span>{{  Bengali::bn_number($article->views) . __(' বার পঠিত') }}</span>

                        </div>
                        <a href="{{ route(strtolower($article->post_type) . 's.show-by-slug', $article->id)}}"
                            class="post-title">
                            {!! Str::words($article->title, 10,'...') !!}
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <p>কোন পোস্ট করা হয়নি...</p>
            @endif
        </div>
    </section>
</div>
@endsection
