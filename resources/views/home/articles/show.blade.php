@extends('layouts.app')

@section('meta')
<meta property="og:title" content="{{ $article->title }}" />
<meta property="og:type" content="article" />
<meta property="og:url" content="{{ route('articles.show-by-slug', $article->slug) }}" />
<meta property="og:image"
    content="{{ $article->thumbnail ? url('storage/uploads/post/featured_image/image_' . $article->thumbnail) : asset('images/no-thumb.jpg') }}" />
<meta property="og:description" content="{!! Str::words($article->body, 60,'...') !!}">
<meta property="og:site_name" content="{{ __('চিন্তাধারা') }}" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@chintadhara" />
<meta name="twitter:creator" content="@chintadhara" />
<meta property="og:url" content="{{ route('articles.show-by-slug', $article->slug) }}" />
<meta property="og:title" content="{{ $article->title }}" />
<meta property="og:description" content="{!! Str::words($article->body, 60,'...') !!}" />
<meta property="og:image"
    content="{{ $article->thumbnail ? url('storage/uploads/post/featured_image/image_' . $article->thumbnail) : asset('images/no-thumb.jpg') }}" />
@endsection

@section('content')
<div class="mt-1">
    <div class="col-sm-12 col-lg-6 mx-auto">
        <article class="single-article mb-3">
            <img class="featured-image"
                src="{{ $article->thumbnail ? url('storage/uploads/post/featured_image/image_' . $article->thumbnail) : asset('images/no-thumb.jpg') }}">
            <div class="category mt-2">
                @if(count($article->categories) > 0)
                @foreach($article->categories as $index => $category)
                <span>
                    <a href="{{ route('categories.show', $category->id) }}">
                        @if( count($article->categories) != $index + 1 )
                        {{ $category->name . __(', ') }}
                        @else
                        {{ $category->name }}
                        @endif
                    </a>
                </span>
                @endforeach
                @endif
            </div>
            <h5 class="article-title mb-3">{{ $article->title }}</h5>
            <div class="article-meta mb-4 pb-4">
                <div class="row">
                    <div class="col-sm-8">
                        <span class="author">
                            @if(count($article->authors) > 0)
                            @foreach($article->authors as $index => $author)
                            <img src="{{ $author->image ? url('storage/uploads/profile_picture/author/image_') . $author->image : asset('images/writer-image.jpg') }}"
                                class="author-image" alt="{{ $author->name }}">
                            <a href="{{ route('authors.show', $author->id) }}">
                                @if( count($article->authors) != $index + 1 )
                                {{ $author->name . __(', ') }}
                                @else
                                {{ $author->name }}
                                @endif
                            </a>
                            @endforeach
                            @else
                            {{ __('চিন্তাধারা') }}
                            @endif
                        </span>
                        <span class="date">
                            {{ Bengali::bn_date(date('d F, Y', strtotime($article->created_at))) }}
                        </span>
                    </div>
                    <div class="col-sm-4 right-align d-none">
                        <a href="#" class="right button button-outline button-rounded">ডাউনলোড</a>
                    </div>
                </div>
            </div>
            <div class="article-body">{!! $article->body !!}</div>
        </article>

        <div class="post-data mb-3">
            <div class="parent">
                <div class="child">
                    <p class="post-view-count">{{  Bengali::bn_number($article->views) . __(' বার পঠিত') }}</p>
                </div>
                <div class="child">
                    @php
                    $url = route(strtolower($article->post_type) . 's.show-by-slug', $article->slug);
                    $title = Str::words($article->title, 10,'...');
                    @endphp
                    <a href="{{ 'http://www.facebook.com/share.php?u=' . $url . '&title=' . $title }}" target="_blank">
                        <img src="{{ asset('icons/facebook.svg') }}" class="svg-icon mr-3" alt="FB">
                    </a>
                    <a href="{{ 'http://twitter.com/intent/tweet?status=' . $title . ' ' . $url }}" target="_blank">
                        <img src="{{ asset('icons/twitter.svg') }}" class="svg-icon mr-3" alt="FB">
                    </a>
                    <a href="{{ 'http://www.linkedin.com/shareArticle?mini=true&url=' . $url . '&title=' . $title }}"
                        target="_blank">
                        <img src="{{ asset('icons/linkedin.svg') }}" class="svg-icon mr-3" alt="FB">
                    </a>
                    <a href="{{ 'whatsapp://send?text=[' . $title . ' / ' . $url . ']' }}" target="_blank">
                        <img src="{{ asset('icons/whatsapp.svg') }}" class="svg-icon mr-3" alt="FB">
                    </a>
                </div>
            </div>
        </div>

        @if(count($article->authors) > 0)
        @foreach($article->authors as $index => $author)
        <section class="pb-3" id="author">
            <div class="section-title mb-3">
                <h4 class="title">লেখক পরিচিতি</h4>
            </div>
            <div class="author">
                <div class="header">
                    <div class="author-image">
                        <a href="{{ route('authors.show', $author->id) }}">
                            <img src="{{ $author->image ? url('storage/uploads/profile_picture/author/image_') . $author->image : asset('images/writer-image.jpg') }}"
                                class="author-mage" alt="{{ $author->name }}">
                        </a>
                    </div>
                    <div class="author-meta pl-3">
                        <a href="{{ route('authors.show', $author->id) }}" class="link">
                            <h4 class="author-name">
                                {{ $author->name ? $author->name : __('চিন্তাধারা') }}
                            </h4>
                        </a>
                        <a href="{{ route('authors.show', $author->id) }}" class="link mr-3">
                            {{ __('প্রোফাইল দেখুন') }}
                        </a>
                        <a href="{{ route('authors.show', $author->id) }}" class="link">
                            {{ __('সকল পোষ্ট') }}
                        </a>
                    </div>
                </div>
                <p class="author-bio pt-3">
                    {!! Str::words($author->bio, 90,'...') !!}
                    <a href="{{ route('authors.show', $author->id) }}" class="link mr-3">
                        {{ __('আরো পড়ুন') }}
                    </a>
                </p>
            </div>
        </section>
        @endforeach
        @endif

        @if(count($article->translators) > 0)
        @foreach($article->translators as $index => $translator)
        <section class="pb-3" id="author">
            <div class="section-title mb-3">
                <h4 class="title">অনুবাদক পরিচিতি</h4>
            </div>
            <div class="author">
                <div class="header">
                    <div class="author-image">
                        <a href="{{ route('translators.show', $translator->id) }}">
                            <img src="{{ $translator->image ? url('storage/uploads/profile_picture/translator/image_') . $translator->image : asset('images/writer-image.jpg') }}"
                                class="author-mage" alt="{{ $translator->name }}">
                        </a>
                    </div>
                    <div class="author-meta pl-3">
                        <a href="{{ route('translators.show', $translator->id) }}" class="link">
                            <h4 class="author-name">
                                {{ $translator->name ? $translator->name : __('চিন্তাধারা') }}
                            </h4>
                        </a>
                        <a href="{{ route('translators.show', $translator->id) }}" class="link mr-3">
                            {{ __('প্রোফাইল দেখুন') }}
                        </a>
                        <a href="{{ route('translators.show', $translator->id) }}" class="link">
                            {{ __('সকল পোষ্ট') }}
                        </a>
                    </div>
                </div>
                <p class="author-bio pt-3">
                    {!! Str::words($translator->bio, 90,'...') !!}
                    <a href="{{ route('translators.show', $translator->id) }}" class="link mr-3">
                        {{ __('আরো পড়ুন') }}
                    </a>
                </p>
            </div>
        </section>
        @endforeach
        @endif

        <section class="pb-3" id="comment">
            <div class="section-title mb-3">
                <h4 class="title">{{ __('মন্তব্য') }}</h4>
                <span class="right">
                    {{ count($article->comments) ? Bengali::bn_number(count($article->comments)) . __(' টি মন্তব্য করা হয়েছে') : __('কোন মন্তব্য নেই') }}
                </span>
            </div>
            @if(Session::has('comment_response'))
            <div class="alert alert-{{ Session::get('comment_response.type') == 'success' ? 'success' : 'danger' }} ">
                {{ Session::get('comment_response.message') }}
            </div>
            @endif
            @if(count($article->comments))
            <div class="comments">
                @foreach($article->comments as $comment)
                <div class="comment">
                    <div class="border-bottom mb-4">
                        <div class="header mb-2">
                            <div class="author-image">
                                <img src="{{ asset('images/writer-image.jpg') }}" alt="{{ $comment->author_name }}">
                            </div>
                            <div class="author-meta pl-3">
                                <h4 class="author-name">
                                    {{ $comment->author_name ? $comment->author_name : __('নাম প্রকাশে অনিচ্ছুক') }}
                                </h4>
                                <span
                                    class="date">{{ Bengali::bn_date_time(date('d F, Y - H:s A', strtotime($comment->created_at))) }}</span>
                            </div>
                        </div>
                        <p class="content-body">
                            {{ $comment->body }}
                        </p>

                        @if(count($comment->replies))
                        <div class="comments ml-5">
                            @foreach($comment->replies as $reply)
                            <div class="comment">
                                <div class="border-bottom mb-4">
                                    <div class="header mb-2">
                                        <div class="author-image">
                                            <img src="{{ asset('images/writer-image.jpg') }}"
                                                alt="{{ $reply->author_name }}">
                                        </div>
                                        <div class="author-meta pl-3">
                                            <h4 class="author-name">
                                                {{ $reply->author_name ? $reply->author_name : __('নাম প্রকাশে অনিচ্ছুক') }}
                                            </h4>
                                            <span
                                                class="date">{{ Bengali::bn_date_time(date('d F, Y - H:s A', strtotime($reply->created_at))) }}</span>
                                        </div>
                                    </div>
                                    <p class="content-body">
                                        {{ $reply->body }}
                                    </p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @endif
                        @if($article->comment_status == 1)
                        <form action="{{ route('comments.reply.store', $comment->id) }}" method="POST" class="ml-5">
                            @csrf
                            <input type="hidden" name="post_id" value="{{ $article->id }}">
                            <div class="form-group">
                                <input type="text" name="author_name" id="author_name" class="form-control"
                                    placeholder="আপনার নাম">
                            </div>
                            <div class="form-group">
                                <textarea name="body" id="body" class="form-control" cols="30" rows="3"
                                    placeholder="আপনার মন্তব্য"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="button button-primary">উত্তর দিন</button>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
            @endif

            @if($article->comment_status == 1)
            <h4>{{ __('একটি মন্তব্য করুন') }}</h4>
            <form action=" {{ route('comments.store') }}" method="POST">
                @csrf
                <input type="hidden" name="post_id" value="{{ $article->id }}">
                <div class="form-group">
                    <input type="text" name="author_name" id="author_name" class="form-control" placeholder="আপনার নাম">
                </div>
                <div class="form-group">
                    <textarea name="body" id="body" class="form-control" cols="30" rows="3"
                        placeholder="আপনার মন্তব্য"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="button button-primary">মন্তব্য করুন</button>
                </div>
            </form>
            @endif
        </section>
    </div>

    <!-- Related Posts -->
    <section class="container" id="comment">
        <div class="section-title mb-3">
            <h4 class="title">{{ __('এ রকম আরও কিছু লিখা') }}</h4>
        </div>
        <div class="related-post">
            <div class="row">
                @if( count($related_articles) )
                @foreach( $related_articles as $related_article )
                <div class="col-sm-12 col-md-6 col-lg-3">
                    <div class="single-post">
                        <div class="card-post">
                            <a href="{{ route('articles.show-by-slug', $related_article->slug) }}">
                                <img src="@if($related_article->thumbnail != null) {{ url('storage/uploads/post/thumbnail/thumb_' . $related_article->thumbnail) }} @else {{ asset('images/no-thumb.jpg')}} @endif"
                                    alt="{{ $related_article->title }}" class="post-thumb">
                            </a>
                            <div class="post-meta">
                                <span class="post-category">
                                    @if(count($related_article->authors) > 0)
                                    <a href="{{ route('authors.show', $related_article->authors[0]->id) }}">
                                        {{ $related_article->authors[0]->name }}
                                    </a>
                                    @else
                                    {{ __('চিন্তাধারা') }}
                                    @endif
                                </span>
                                <span class="post-date">
                                    {{  Bengali::bn_number($related_article->views) . __(' বার পঠিত') }}
                                </span>
                            </div>
                            <a href="{{ route('articles.show-by-slug', $related_article->slug) }}">
                                <h5 class="post-title">{!! Str::words($related_article->title, 10,'...') !!}</h5>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <p>কোন পোস্ট পাওয়া যায়নি...</p>
                @endif
            </div>
        </div>
    </section>
</div>
@endsection
