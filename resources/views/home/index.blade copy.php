@extends('layouts.app')

@section('content')

<section class="slider-area pt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="slider-active">
                    <div class="slider-item slider-bg-1">
                        <div class="slider-content">
                            <h2>The holy Quran</h2>
                            <h6>Chinta Dhara</h6>
                        </div>
                    </div>
                    <div class="slider-item slider-bg-2">
                        <div class="slider-content">
                            <h2>The holy Quran</h2>
                            <h6>Chinta Dhara</h6>
                        </div>
                    </div>
                    <div class="slider-item slider-bg-3">
                        <div class="slider-content">
                            <h2>The holy Quran</h2>
                            <h6>Chinta Dhara</h6>
                        </div>
                    </div>
                    <div class="slider-item slider-bg-4">
                        <div class="slider-content">
                            <h2>The holy Quran</h2>
                            <h6>Chinta Dhara</h6>
                        </div>
                    </div>
                    <div class="slider-item slider-bg-5">
                        <div class="slider-content">
                            <h2>The holy Quran</h2>
                            <h6>Chinta Dhara</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sidebar-wrapper">
                    <div class="sidebar-content">
                        <h4>Populer</h4>
                        <ul>
                            <li>
                                <a href="#">
                                    <h5>The holy Quran</h5>
                                </a>
                                <h6>The holy Quran</h6>
                            </li>
                            <li>
                                <a href="#">
                                    <h5>The holy Quran</h5>
                                </a>
                                <h6>The holy Quran</h6>
                            </li>
                            <li>
                                <a href="#">
                                    <h5>The holy Quran</h5>
                                </a>
                                <h6>The holy Quran</h6>
                            </li>
                            <li>
                                <a href="#">
                                    <h5>The holy Quran</h5>
                                </a>
                                <h6>The holy Quran</h6>
                            </li>
                            <li>
                                <a href="#">
                                    <h5>The holy Quran</h5>
                                </a>
                                <h6>The holy Quran</h6>
                            </li>
                            <li>
                                <a href="#">
                                    <h5>The holy Quran</h5>
                                </a>
                                <h6>The holy Quran</h6>
                            </li>
                            <li>
                                <a href="#">
                                    <h5>The holy Quran</h5>
                                </a>
                                <h6>The holy Quran</h6>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container mt-5 home-content-type-list">
    <a href="{{ url('/') }}" class="button button-outline button-rounded">সবগুলো</a>
    <a href="{{ url('/articles') }}" class="button button-outline button-rounded">প্রবন্ধ</a>
    <!-- <a href="{{ url('/videos') }}" class="button button-outline button-rounded">ভিডিও</a>
    <a href="{{ url('/podcasts') }}" class="button button-outline button-rounded">পডকাস্ট</a> -->
    <a href="{{ url('/authors') }}" class="button button-outline button-rounded">লেখক</a>
    <!-- <a href="{{ url('/translators') }}" class="button button-outline button-rounded">অনুবাদক</a>
    <a href="{{ url('/socials') }}" class="button button-outline button-rounded">স্যোশাল</a> -->
</div>

<div class="container mt-5 pb-5">
    <section class="home-recent-posts">
        <div class="row">
            @if( isset($recent_posts) )
            @foreach( $recent_posts as $recent_post )
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="single-post">
                    <div class="card-post">
                        <a href="{{ route($recent_post->post_type . 's.show-by-slug', $recent_post->slug) }}">
                            <img src="@if($recent_post->thumbnail != null) {{ url('storage/uploads/post/thumbnail/thumb_' . $recent_post->thumbnail) }} @else {{ asset('images/no-thumb.jpg')}} @endif"
                                alt="{!! Str::words($recent_post->title, 10,'...') !!}" class="post-thumb">
                        </a>
                        <div class="post-meta">
                            <span class="post-author">
                                @if(count($recent_post->authors) > 0)
                                <a href="{{ route('authors.show', $recent_post->authors[0]->id) }}">
                                    {{ $recent_post->authors[0]->name }}
                                </a>
                                @elseif(count($recent_post->translators) > 0)
                                <a href="{{ route('translators.show', $recent_post->translators[0]->id) }}">
                                    {{ $recent_post->translators[0]->name }}
                                </a>
                                @else
                                {{ __('চিন্তাধারা') }}
                                @endif
                            </span>
                            <span>{{ Bengali::bn_number($recent_post->views) . __(' বার পঠিত') }}</span>

                        </div>
                        <a href="{{ route($recent_post->post_type . 's.show-by-slug', $recent_post->slug) }}"
                            class="post-title">
                            {!! Str::words($recent_post->title, 10,'...') !!}
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <p>কোন পোস্ট করা হয়নি...</p>
            @endif
        </div>
    </section>
</div>

@if(isset( $cat_1['posts']) || isset($cat_1['title']['name']))
<section class="home-section orange">
    <div class="container">
        <a href="#" class="cat-title">
            <h4>{{ $cat_1['title']['name'] }}</h4>
        </a>
        <section class="home-recent-posts">
            @if( isset($cat_1['posts']) && count($cat_1['posts']) )
            <div class="row">
                @foreach( $cat_1['posts'] as $post )
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="single-post">
                        <div class="card-post">
                            <a href="{{ route($post->post_type . 's.show-by-slug', $post->slug)}}">
                                <img src="@if($post->thumbnail != null) {{ url('storage/uploads/post/thumbnail/thumb_' . $post->thumbnail) }} @else {{ asset('images/no-thumb.jpg')}} @endif"
                                    alt="{!! Str::words($post->title, 10,'...') !!}" class="post-thumb">
                            </a>
                            <div class="post-meta">
                                <span class="post-author">
                                    @if(count($post->authors) > 0)
                                    <a href="{{ route('authors.show', $post->authors[0]->id) }}">
                                        {{ $post->authors[0]->name }}
                                    </a>
                                    @elseif(count($post->translators) > 0)
                                    <a href="{{ route('translators.show', $post->translators[0]->id) }}">
                                        {{ $post->translators[0]->name }}
                                    </a>
                                    @else
                                    {{ __('চিন্তাধারা') }}
                                    @endif
                                </span>
                                <span>{{  Bengali::bn_number($post->views) . __(' বার পঠিত') }}</span>

                            </div>
                            <a href="{{ route($post->post_type . 's.show-by-slug', $post->slug)}}" class="post-title">
                                {!! Str::words($post->title, 10,'...') !!}
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <p>কোন পোস্ট করা হয়নি...</p>
            @endif
        </section>
    </div>
</section>
@endif

@if(isset( $cat_2['posts']) || isset($cat_2['title']['name']))
<section class="home-section">
    <div class="container">
        <a href="#" class="cat-title">
            <h4>{{ $cat_2['title']['name'] }}</h4>
        </a>
        <section class="home-recent-posts">
            @if( isset($cat_2['posts']) && count($cat_2['posts']) )
            <div class="row">
                @foreach( $cat_2['posts'] as $post )
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="single-post">
                        <div class="card-post">
                            <a href="{{ route($post->post_type . 's.show-by-slug', $post->slug)}}">
                                <img src="@if($post->thumbnail != null) {{ url('storage/uploads/post/thumbnail/thumb_' . $post->thumbnail) }} @else {{ asset('images/no-thumb.jpg')}} @endif"
                                    alt="{!! Str::words($post->title, 10,'...') !!}" class="post-thumb">
                            </a>
                            <div class="post-meta">
                                <span class="post-author">
                                    @if(count($post->authors) > 0)
                                    <a href="{{ route('authors.show', $post->authors[0]->id) }}">
                                        {{ $post->authors[0]->name }}
                                    </a>
                                    @elseif(count($post->translators) > 0)
                                    <a href="{{ route('translators.show', $post->translators[0]->id) }}">
                                        {{ $post->translators[0]->name }}
                                    </a>
                                    @else
                                    {{ __('চিন্তাধারা') }}
                                    @endif
                                </span>
                                <span>{{  Bengali::bn_number($post->views) . __(' বার পঠিত') }}</span>

                            </div>
                            <a href="{{ route($post->post_type . 's.show-by-slug', $post->slug)}}" class="post-title">
                                {!! Str::words($post->title, 10,'...') !!}
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <p>কোন পোস্ট করা হয়নি...</p>
            @endif
        </section>
    </div>
</section>
@endif

@if(isset( $cat_3['posts']) || isset($cat_3['title']['name']))
<section class="home-section reverse">
    <div class="container">
        <a href="#" class="cat-title">
            <h4>{{ $cat_3['title']['name'] }}</h4>
        </a>
        <section class="home-recent-posts">
            @if( isset($cat_3['posts']) && count($cat_3['posts']) )
            <div class="row">
                @foreach( $cat_3['posts'] as $post )
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="single-post">
                        <div class="card-post">
                            <a href="{{ route($post->post_type . 's.show-by-slug', $post->slug)}}">
                                <img src="@if($post->thumbnail != null) {{ url('storage/uploads/post/thumbnail/thumb_' . $post->thumbnail) }} @else {{ asset('images/no-thumb.jpg')}} @endif"
                                    alt="{!! Str::words($post->title, 10,'...') !!}" class="post-thumb">
                            </a>
                            <div class="post-meta">
                                <span class="post-author">
                                    @if(count($post->authors) > 0)
                                    <a href="{{ route('authors.show', $post->authors[0]->id) }}">
                                        {{ $post->authors[0]->name }}
                                    </a>
                                    @elseif(count($post->translators) > 0)
                                    <a href="{{ route('translators.show', $post->translators[0]->id) }}">
                                        {{ $post->translators[0]->name }}
                                    </a>
                                    @else
                                    {{ __('চিন্তাধারা') }}
                                    @endif
                                </span>
                                <span>{{  Bengali::bn_number($post->views) . __(' বার পঠিত') }}</span>

                            </div>
                            <a href="{{ route($post->post_type . 's.show-by-slug', $post->slug)}}" class="post-title">
                                {!! Str::words($post->title, 10,'...') !!}
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @else
            <p>কোন পোস্ট করা হয়নি...</p>
            @endif
        </section>
    </div>
</section>
@endif

<section class="home-section">
    <div class="container">
        <a href="#" class="cat-title">
            <h4>লেখকবৃন্দ</h4>
        </a>
        <div class="home-authors text-center">
            <div class="row">
                @foreach($authors as $index => $author)
                <div class="col-6 col-sm-4 col-md-3 col-lg-2 mb-3">
                    <div class="author">
                        <a href="{{ route('authors.show', $author->id) }}">
                            <img src="{{ $author->image ? url('storage/uploads/author/profile_picture/image_') . $author->image : asset('images/writer-image.jpg') }}"
                                alt="{{ $author->name }}">
                        </a>
                        <h3 class="author-name">
                            <a href="{{ route('authors.show', $author->id) }}">
                                {{ $author->name }}
                            </a>
                        </h3>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

<style>
    /* slider css */
    .slider-item {
        height: 350px;
        max-height: 350px;
        background-image: url("{{ asset('images/slide/slider-1.jpg') }}");
        z-index: 2;
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        position: relative;
    }

    .slider-bg-2 {
        background-image: url("{{ asset('images/slide/slider-2.jpg') }}");
    }

    .slider-bg-3 {
        background-image: url("{{ asset('images/slide/slider-3.jpg') }}");
    }

    .slider-bg-4 {
        background-image: url("{{ asset('images/slide/slider-4.jpg') }}");
    }

    .slider-bg-5 {
        background-image: url("{{ asset('images/slide/slider-5.jpg') }}");
    }

</style>
@endsection
