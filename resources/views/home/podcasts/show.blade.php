@extends('layouts.app')

@section('content')
<div class="mt-1">
    <div class="col-sm-12 col-lg-6 mx-auto">
        <article class="single-article">
            <img class="featured-image" src="{{ asset('img/no-thumb.png') }}">

            <h5 class="article-title">{{ $post->title }}</h5>
            <div class="article-meta">
                <span class="">লেখকঃ {{ $post->user->name }}</span>
            </div>
            <p class="article-body">{{ $post->body }}</p>

        </article>
    </div>
    <section class="container">
        <h4 class="contet-title">এরকম আরো কিছু লিখা</h4>
        <div class="related-post">
            <div class="row">
                @foreach( $related_posts as $related_post )
                <div class="col-sm-12 col-md-6 col-lg-3">
                    <div class="single-post">
                        <div class="card-post">
                            <a href="{{ route('posts.show', $related_post->id) }}">
                                <img src="{{ asset('img/no-thumb.png') }}" alt="{{ $related_post->title }}"
                                    class="post-thumb">
                            </a>
                            <a href="{{ route('posts.show', $related_post->id) }}">
                                <h5 class="article-title">{!! Str::words($related_post->title, 10,'...') !!}</h5>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
</div>
@endsection
