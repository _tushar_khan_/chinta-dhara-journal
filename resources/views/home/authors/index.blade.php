@extends('layouts.app')

@section('content')
<section class="container mt-5">
    <div class="section-title mb-3">
        <h4 class="title">{{ __('চিন্তাধারা-র লেখকবৃন্দ') }}</h4>
    </div>
    <div class="home-authors text-center">
        <div class="row">
            @foreach($authors as $index => $author)
            <div class="col-6 col-sm-4 col-md-3 col-lg-2 mb-3">
                <div class="author">
                    <a href="{{ route('authors.show', $author->id) }}">
                        <img src="{{ $author->image ? url('storage/uploads/profile_picture/author/image_') . $author->image : asset('images/writer-image.jpg') }}"
                            alt="{{ $author->name }}">
                    </a>
                    <h3 class="author-name">
                        <a href="{{ route('authors.show', $author->id) }}">{{ $author->name }}</a>
                    </h3>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
