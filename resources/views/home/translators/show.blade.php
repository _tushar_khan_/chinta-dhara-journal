@extends('layouts.app')

@section('content')
<section class="profile-banner pt-3">
    <div class="container">
        <div class="profile-info">
            <img src="{{ $translator->image ? url('storage/uploads/profile_picture/translator/image_') . $translator->image : asset('images/writer-image.jpg') }}"
                alt="{{ $translator->name }}" class="profile-img">
            <h2 class="author-name">{{ $translator->name }}</h2>
        </div>
    </div>
</section>
<section class="container mt-5">
    <p class="author-bio">
        {{ $translator->bio }}
    </p>
    <div class="section-title mb-3">
        <h4 class="title">{{ __('অনুবাদকের প্রকাশিত পোষ্টগুলো') }}</h4>
    </div>
    @if( isset($posts) && count($posts) > 0)
    <div class="home-recent-posts">
        <div class="row">
            @foreach( $posts as $post )
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="single-post">
                    <div class="card-post">
                        <a href="{{ route(strtolower($post->post_type) . 's.show-by-slug', $post->slug)}}">
                            <img src="@if($post->thumbnail != null) {{ url('storage/uploads/post/thumbnail/thumb_' . $post->thumbnail) }} @else {{ asset('images/no-thumb.jpg')}} @endif"
                                alt="{!! Str::words($post->title, 10,'...') !!}" class="post-thumb">
                        </a>
                        <div class="post-meta">
                            <span class="post-author">
                                @if(count($post->categories) > 0)
                                <a href="{{ route('categories.show', $post->categories[0]->id) }}">
                                    {{ $post->categories[0]->name }}
                                </a>
                                @endif
                            </span>
                            <span>{{  Bengali::bn_number($post->views) . __(' বার পঠিত') }}</span>

                        </div>
                        <a href="{{ route(strtolower($post->post_type) . 's.show-by-slug', $post->slug)}}"
                            class="post-title">
                            {!! Str::words($post->title, 10,'...') !!}
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @else
    <p>{{ __('এই লেখকের কোন পোস্ট নেই...') }}</p>
    @endif
</section>
@endsection
