@extends('layouts.app')

@section('content')
<section class="container mt-5">
    <div class="section-title mb-3">
        <h4 class="title">{{ __('চিন্তাধারা-র অনুবাদকবৃন্দ') }}</h4>
    </div>
    <div class="home-authors text-center">
        <div class="row">
            @foreach($translators as $index => $translator)
            <div class="col-6 col-sm-4 col-md-3 col-lg-2 mb-3">
                <div class="author">
                    <a href="{{ route('translators.show', $translator->id) }}">
                        <img src="{{ $translator->image ? url('storage/uploads/profile_picture/translator/image_') . $translator->image : asset('images/writer-image.jpg') }}"
                            alt="{{ $translator->name }}">
                    </a>
                    <h3 class="author-name">
                        <a href="{{ route('translators.show', $translator->id) }}">{{ $translator->name }}</a>
                    </h3>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection
