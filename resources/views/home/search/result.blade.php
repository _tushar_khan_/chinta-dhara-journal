@extends('layouts.app')

@section('content')
<section class="container mt-5">
    <div class="section-title mb-3">
        <h4 class="title">{{ __('অনুসন্ধানের ফলাফল - ') . $key }}</h4>
    </div>
    <div class="home-recent-posts">
        @if( isset($results['posts']) && count($results['posts']) != 0 )
        <div class="row">
            @foreach( $results['posts'] as $index => $post )
            <div class="col-sm-12 col-md-6 col-lg-4">
                <div class="single-post">
                    <div class="card-post">
                        <a href="{{ route(strtolower($post->post_type) . 's.show-by-slug', $post->slug) }}">
                            <img src="@if($post->thumbnail != null) {{ url('storage/uploads/post/thumbnail/thumb_' . $post->thumbnail) }} @else {{ asset('images/no-thumb.jpg')}} @endif"
                                alt="{!! Str::words($post->title, 10,'...') !!}" class="post-thumb">
                        </a>
                        <div class="post-meta">
                            <span class="post-author">
                                @if(count($post->authors) > 0)
                                <a href="{{ route('authors.show', $post->authors[0]->id) }}">
                                    {{ $post->authors[0]->name }}
                                </a>
                                @elseif(count($post->translators) > 0)
                                <a href="{{ route('translators.show', $post->translators[0]->id) }}">
                                    {{ $post->translators[0]->name }}
                                </a>
                                @else
                                {{ __('চিন্তাধারা') }}
                                @endif
                            </span>
                            <span>{{ $post->views . __(' বার পঠিত') }}</span>

                        </div>
                        <a href="{{ route(strtolower($post->post_type) . 's.show-by-slug', $post->slug)}}"
                            class="post-title">
                            {!! Str::words($post->title, 10,'...') !!}
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <p>কোন পোস্ট পাওয়া যায়নি...</p>
        @endif
    </div>
</section>
{{--

@if(count($results['categories']))
<h3>{{ __('ক্যাটাগরি সমূহ') }}</h3>
@foreach($results['categories'] as $index => $category)
{{ $category }}
@endforeach
@endif

@if(count($results['authors']))
<h3>{{ __('লেখকবৃন্দ') }}</h3>
@foreach($results['authors'] as $index => $author)
{{ $author }}
@endforeach
@endif

@if(count($results['translators']))
<h3>{{ __('অনুবাদকবৃন্দ') }}</h3>
@foreach($results['translators'] as $index => $translator)
{{ $translator }}
@endforeach
@endif --}}
@endsection
