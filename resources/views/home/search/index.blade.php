@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="m1-2 search">
        <h3 class="cat-page-title">{{ __('অনুসন্ধান করুন') }}</h3>
        <div class="search-box-full">
            <form action="{{ route('search.result') }}" method="GET" class="form-inline">
                <input type="text" name="query" id="query" class="form-control search-input mr-1"
                    placeholder="অনুসন্ধান করুন...">
                <button type="submit" class="button button-outline search-button"><i
                        class="material-icons">search</i></button>
            </form>
        </div>
    </div>
</div>
<style>
    .form-inline {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
        -ms-flex-align: center;
        align-items: center;
    }

    .search-box-full .form-inline .search-input {
        width: calc(100% - 55px);
        padding: 10px;
        box-shadow: none;
        border-radius: 3px;
        border: 1px solid #ddd;
        font-size: 1.2rem;
    }

    .search-button {
        position: relative;
        right: 0;
        padding: 9px;
        height: 50px;
    }

    .search-button i {
        line-height: 2rem;
        font-size: 1.9rem;
        padding: 0px;
    }

</style>
@endsection
