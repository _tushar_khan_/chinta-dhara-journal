<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@hasSection('title') @yield('title') - @endif {{ config('app.name', 'চিন্তাধারা') }}</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">

    @hasSection('meta') @yield('meta') @endif
    <meta property="og:locale" content="{{ str_replace('_', '-', app()->getLocale()) }}" />

    <!-- Styles -->
    <link href="{{ asset('css/reboot.css') }}" rel="stylesheet">
    <link href="{{ asset('css/grid.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>

<body>
    <header>
        <div class="navbar navbar-fixed" id="main-nav">
            <div class="nav-top">
                <div class="container">
                    <div class="row">
                        <div class="mr-auto">
                            <a href="{{ route('search.index') }}" id="search" class="nav-link-icon">
                                <i class="material-icons">search</i>
                            </a>
                        </div>
                        <div class="logo nav-m-auto">
                            <a href="{{ url('/') }}">
                                <img src="{{ asset('images/logo.png') }}" alt="">
                            </a>
                        </div>
                        <div class="ml-auto">
                            <a href=" {{ route('post_box') }}" class="nav-link-icon">
                                <i class="material-icons">drafts</i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="navbar-menu">
                <div class="container">
                    <div class="row">
                        <ul class="navbar-nav mr-auto" id="primary-nav">
                            <li class="nav-item">
                                <a href="{{ url('/') }}" class="nav-link home">
                                    প্রচ্ছদ
                                </a>
                            </li>
                            @php $categories = App\Models\Category::active()->limit(10)->get(); @endphp
                            @if( isset($categories) && count($categories) > 0)
                            @foreach( $categories as $category )
                            <li class="nav-item">
                                <a href="{{url('categories/' . $category->id)}}" class="nav-link">
                                    {{$category->name}}
                                </a>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a href="javascript:void();" class="nav-link" id="show-dropdown">
                                    সবগুলো দেখুন
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="dropdown-menu">
                    <div class="container">
                        <ul class="row">
                            @php $categories = App\Models\Category::active()->get(); @endphp
                            @if( isset($categories) && count($categories) > 0)
                            @foreach( $categories as $category )
                            <li class="col-12 col-lg-2">
                                <a href="{{url('categories/' . $category->id)}}">
                                    {{$category->name}}
                                </a>
                            </li>
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main class="main-app">
        @yield('content')
    </main>

    <footer class="footer">
        <div class="footer-menu">
            <ul>
                <li><a href="#">আমাদের সম্পর্কে</a></li>
                <li><a href="#">যোগাযোগ</a></li>
                <li><a href=" {{ route('post_box') }}">আপনিও লিখুন</a></li>
                <li><a href="#">প্রশ্ন-উত্তর</a></li>
            </ul>
        </div>
        <div class="footer-bottom">
            <div class="container" style="display:flex; justify-content: space-between">
                <p>© স্বত্ব চিন্তাধারা {{ Bengali::bn_date(date('Y')) }}</p>
                <p class="social-links">
                    <a href="#"><i class="material-icons">live_tv</i></a>
                    <a href="#"><i class="material-icons">mail</i></a>
                </p>
                <a href="#" class="scroll-up"><i class="material-icons">arrow_upward</i></a>
            </div>
        </div>
    </footer>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        $('#show-dropdown').click(function () {
            $('.navbar-menu').toggleClass('show-dropdown');
        });
        var prevScrollpos = window.pageYOffset;
        window.onscroll = function () {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("main-nav").style = "transform: translateY(0px);";
            } else {
                document.getElementById("main-nav").style = "transform: translateY(-100%);";
            }
            prevScrollpos = currentScrollPos;

            $('.navbar-menu').removeClass('show-dropdown');
        }

    </script>
</body>

</html>
