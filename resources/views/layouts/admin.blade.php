<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> @yield('title') - {{ config('app.name', 'Admin') }}</title>
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">

    <!-- CSS -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/dashboard/materialize.css') }}" type="text/css" rel="stylesheet"
        media="screen,projection">
    <link href="{{ asset('css/dashboard/style.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/dashboard/custom.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/dashboard/app.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/dashboard/dashboard.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/dashboard/material-data-table.css') }}" type="text/css" rel="stylesheet"
        media="screen,projection">

    <!-- Scripts -->
    <script src="{{ asset('js/dashboard/jquery-2.1.1.min.js') }}"></script>
</head>

<body>
    <header class="fixed-header">
        <nav class="half primary no-shadow" role="navigation">
            <div class="nav-wrapper container-fluid">
                <a id="logo-container" href="{{ url('admin/dashboard') }}" class="brand-logo">
                    {{ config('app.name', 'Smart ISP Billing') }}
                </a>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a href="{{ url('admin/dashboard') }}">HOME</a>
                    </li>
                    <li>
                        <a href="{{ url('/') }}" target="_blank">SITE</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/posts') }}">POST</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/admins') }}">ADMIN</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"
                            style="display: none;">
                            @csrf
                        </form>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="profile-avatar" data-activates="profile-dropdown">
                            <img src="{{asset('images/avatar.png')}}">
                        </a>
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li>
                                <a href="#" class="grey-text text-darken-1">
                                    <i class="material-icons">face</i>
                                    Profile
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul id="nav-mobile" class="sidenav">
                    <li>
                        <a href="#">Link</a>
                    </li>
                </ul>
            </div>
        </nav>
        <nav class="half white" role="navigation">
            <div class="nav-wrapper container">
                <a class="nav-title">
                    @hasSection('page-title')
                    @yield('page-title')
                    @else
                    @hasSection('title')
                    @yield('title')
                    @else
                    Dashboard
                    @endif
                    @endif
                </a>
                @hasSection('second-nav-bar')
                @yield('second-nav-bar')
                @endif
            </div>
        </nav>
    </header>
    <div class="main">
        <aside class="side-nav">
            <ul class="collapsible" data-collapsible="accordion">
                <li class="bold">
                    <a href="{{ url('/admin/dashboard') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">dashboard</i>
                        <span class="nav-text">Dashboard</span>
                    </a>
                </li>
                <li class="bold">
                    <a href="{{ url('admin/articles') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">assignment</i>
                        <span class="nav-text">Articles</span>
                    </a>
                </li>
                <li class="bold">
                    <a href="{{ url('admin/articles/create') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">add</i>
                        <span class="nav-text">Add Article</span>
                    </a>
                </li>
                <li class="bold">
                    <a href="{{ url('admin/categories') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">category</i>
                        <span class="nav-text">Categories</span>
                    </a>
                </li>
                <li class="bold">
                    <a href="{{ url('admin/categories/create') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">add</i>
                        <span class="nav-text">Add Category</span>
                    </a>
                </li>
                <li class="bold">
                    <a href="{{ url('admin/authors') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">wc</i>
                        <span class="nav-text">Authors</span>
                    </a>
                </li>
                <li class="bold">
                    <a href="{{ url('admin/authors/create') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">add</i>
                        <span class="nav-text">Add Author</span>
                    </a>
                </li>

                <li class="bold">
                    <a href="{{ url('admin/translators') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">local_library</i>
                        <span class="nav-text">Translators</span>
                    </a>
                </li>
                <li class="bold">
                    <a href="{{ url('admin/translators/create') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">add</i>
                        <span class="nav-text">Add Translator</span>
                    </a>
                </li>
                <li class="bold">
                    <a href="{{ url('admin/post_requests') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">speaker_notes</i>
                        <span class="nav-text">Post Requests</span>
                    </a>
                </li>
                <!-- <li class="bold">
                    <a href="{{ url('admin/tags') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">style</i>
                        <span class="nav-text">Tags</span>
                    </a>
                </li>
                <li class="bold">
                    <a href="{{ url('admin/tags/create') }}" class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">add</i>
                        <span class="nav-text">Add Tag</span>
                    </a>
                </li> -->
            </ul>
        </aside>
        <section class="content">
            <div class="container">@yield('content')</div>
        </section>
    </div>

    <footer class="page-footer primary">
        <div class="footer-copyright">
            <div class="container">
                <div class="left">
                    <p class="m-0">
                        {{ config('app.name', 'Smart ISP Billing') }}, Version: {{ env('APP_VERSION')}}
                    </p>
                </div>
                <div class="right">
                    <p class="m-0">Developed by DevXpart</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scripts -->
    <script src="{{ asset('js/dashboard/materialize.js') }}"></script>
    <script src="{{ asset('js/dashboard/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/12.2.0/classic/ckeditor.js"></script>
    <script src="{{ asset('js/dashboard/material-data-table.js') }}"></script>
    <script src="{{ asset('js/dashboard/init.js') }}"></script>
    <script src="{{ asset('js/dashboard/script.js') }}"></script>
    @if(Session::has('response'))
    <script>
        var toastData = {
            html: '{{Session::get("response.message")}}',
            classes: '{{Session::get("response.type") == "success" ? "green" : "red"}}'
        };
        M.toast(toastData);

    </script>
    @endif
</body>

</html>
