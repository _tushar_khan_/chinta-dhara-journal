$(document).ready(function () {
    $('.material-select').formSelect();
    $('.tooltipped').tooltip();
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('.tabs').tabs();

});
document.addEventListener('DOMContentLoaded', function () {
    var elements = document.querySelectorAll('.modal');
    options = '';
    M.Modal.init(elements, options);
});
